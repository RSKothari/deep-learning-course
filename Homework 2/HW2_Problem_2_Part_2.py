from __future__ import division, print_function

import pickle
import sys

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
from scipy import io
from torch.autograd import Variable
from torchvision import models

import cv2

PATH_TO_HOMEWORK_REPO = '/home/rakshit/Documents/DL course'
sys.path.insert(0, PATH_TO_HOMEWORK_REPO)

from My_NN_package.My_Neural_Network import Neural_Network

MODEL_VGG16 = models.vgg16(pretrained=True)
MODEL_VGG16.double()
MODEL_VGG16.eval()

# The reason we separate them is because of a pooling operation
MODEL_VGG16_FEATURES = nn.Sequential(*list(MODEL_VGG16.features.children()))
MODEL_VGG16_CLASSIFIER = nn.Sequential(*list(MODEL_VGG16.classifier.children())[:-3])
MODEL_VGG16_FEATURES.eval()
MODEL_VGG16_CLASSIFIER.eval()
MODEL_VGG16_FEATURES.cuda()
MODEL_VGG16_CLASSIFIER.cuda()

PATH_TO_HOMEWORK_REPO = '/home/rakshit/Documents/DL course'
PATH_TO_IMAGE_DATABASE = PATH_TO_HOMEWORK_REPO + '/Data/Pets dataset'
TRAINDATA = io.loadmat(PATH_TO_IMAGE_DATABASE + '/PetsTrain.mat')
TESTDATA = io.loadmat(PATH_TO_IMAGE_DATABASE + '/PetsTest.mat')

TRAIN_FILE_NAMES = TRAINDATA['files']
TEST_FILE_NAMES = TESTDATA['files']

TRAIN_FEATURES = []
TEST_FEATURES = []

batch_size = 10

''' IP = np.zeros((len(TRAIN_FILE_NAMES), 3, 254, 254), dtype=float)
# Extract training features from VGG16 without final 3 layers
print("Number of training images:", len(TRAIN_FILE_NAMES))
for i in range(0, len(TRAIN_FILE_NAMES)):
    IMG = cv2.imread(PATH_TO_IMAGE_DATABASE + '/images/' + TRAIN_FILE_NAMES[i][0][0], 1)
    # Preprocess the image
    IMG = IMG[:, :, ::-1]
    IMG = cv2.resize(IMG, (254, 254), interpolation=cv2.INTER_CUBIC)
    IMG = IMG.astype(np.float)/255
    IMG = (IMG - np.array([0.485, 0.456, 0.406]))/np.array([0.229, 0.224, 0.225])
    IMG = np.swapaxes(IMG, 0, 2)
    IMG = np.swapaxes(IMG, 1, 2)
    IP[i, :, :, :] = IMG

    if (i+1) % batch_size == 0 or i == len(TRAIN_FILE_NAMES) - 1:
        if (i+1) % batch_size == 0:
            xprev = i - batch_size + 1
        elif i == len(TRAIN_FILE_NAMES) - 1:
            xprev = i - i%batch_size

        print("Batch:", xprev, " to", i)
        TEMP = torch.from_numpy(IP[xprev:i+1, :, :, :]).cuda()
        OUT = MODEL_VGG16_FEATURES.forward(Variable(TEMP, requires_grad=False))
        OUT = OUT.view(OUT.size(0), -1)
        OUT = MODEL_VGG16_CLASSIFIER(OUT).cpu()
        print("Batch output shape", OUT.data.numpy().shape)
        TRAIN_FEATURES.append(OUT.data.numpy())

TRAIN_FEATURES = np.vstack(TRAIN_FEATURES)
print(TRAIN_FEATURES.shape)

# Write out features in pickle file for future
pickle.dump([TRAIN_FEATURES, TRAINDATA['label']],
            open("pets_training_features.p", "wb"))
 '''
''' # Testing data

IP = np.zeros((len(TEST_FILE_NAMES), 3, 254, 254), dtype=float)

# Extract testing features from VGG16 without final 3 layers
print("Number of testing images:", len(TEST_FILE_NAMES))
for i in range(0, len(TEST_FILE_NAMES)):
    IMG = cv2.imread(PATH_TO_IMAGE_DATABASE + '/images/' + TEST_FILE_NAMES[i][0][0], 1)

    # Preprocess the image
    IMG = cv2.resize(IMG, (254, 254), interpolation=cv2.INTER_CUBIC)
    IMG = IMG.astype(np.float)/255
    IMG = (IMG - np.array([0.485, 0.456, 0.406]))/np.array([0.229, 0.224, 0.225])
    IMG = np.swapaxes(IMG, 0, 2)
    IMG = np.swapaxes(IMG, 1, 2)
    IP[i, :, :, :] = IMG

    if (i+1) % batch_size == 0 or i == len(TEST_FILE_NAMES) - 1:
        if (i+1) % batch_size == 0:
            xprev = i - batch_size + 1
        elif i == len(TRAIN_FILE_NAMES) - 1:
            xprev = i - i%batch_size

        print("Batch:", xprev, " to", i)
        TEMP = torch.from_numpy(IP[xprev:i+1, :, :, :]).cuda()
        OUT = MODEL_VGG16_FEATURES.forward(Variable(TEMP, requires_grad=False))
        OUT = OUT.view(OUT.size(0), -1)
        OUT = MODEL_VGG16_CLASSIFIER(OUT).cpu()
        print("Batch output shape", OUT.data.numpy().shape)
        TEST_FEATURES.append(OUT.data.numpy())

TEST_FEATURES = np.vstack(TEST_FEATURES)
print(TEST_FEATURES.shape)

pickle.dump([TEST_FEATURES, TESTDATA['label']],
            open("pets_test_features.p", "wb")) '''

TRAIN_FEATURES, TRAIN_LABELS = pickle.load(open("pets_training_features.p", "rb"))
TEST_FEATURES, TEST_LABELS = pickle.load(open("pets_test_features.p", "rb"))

def plot_data(line_x_list, line_y_list, label_list, title_str):
    """Plotting functions for training and testing loss and mpca"""
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.title(title_str)
    for i in range(0, len(line_y_list)):
        ax.plot(line_x_list[i], line_y_list[i], label=label_list[i])
    colormap = plt.cm.brg #nipy_spectral, Set1,Paired
    colors = [colormap(i) for i in np.linspace(0, 1, len(ax.lines))]
    for i, j in enumerate(ax.lines):
        j.set_color(colors[i])
    ax.legend(loc=0)
    plt.show() 

#Normalize TRAIN_FEATURES
TRAIN_FEATURES = TRAIN_FEATURES/np.tile(np.linalg.norm(TRAIN_FEATURES, ord=None, axis=1), (4096, 1)).T
TEST_FEATURES = TEST_FEATURES/np.tile(np.linalg.norm(TEST_FEATURES, ord=None, axis=1), (4096, 1)).T
MEAN_TRAIN = np.mean(TRAIN_FEATURES, 0)

STAT_SET = {} # Define an empty dictionary
STAT_SET['network_inputs'] = TRAIN_FEATURES - MEAN_TRAIN
STAT_SET['network_targets'] = TRAIN_LABELS
STAT_SET['test_inputs'] = TEST_FEATURES - MEAN_TRAIN
STAT_SET['test_targets'] = TEST_LABELS
STAT_SET['num_hidden_layers'] = 0
STAT_SET['num_hidden_nodes'] = 10
STAT_SET['list_activation_functions'] = ['sigmoid', 'sigmoid']
STAT_SET['batch_size'] = 30
STAT_SET['num_epochs'] = 25
STAT_SET['learning_rate'] = [1.*1e-3, 50.*1e-3]
STAT_SET['learning_rate_function'] = 'gaussian'
STAT_SET['reg_rate'] = 0.009
STAT_SET['dropout_rate'] = 0
STAT_SET['momentum_function'] = 'nesterov'
STAT_SET['momentum'] = 0.8
STAT_SET['dropout'] = False
STAT_SET['dropout_layer_idx'] = [1,]

#NET = Neural_Network()
#NET.initialize_network(STAT_SET)
#TRAIN_RESULTS, TEST_RESULTS, TEST_CONFUSION = NET.Train(0)

#pickle.dump([TRAIN_RESULTS, TEST_RESULTS, TEST_CONFUSION],
#            open("Problem_2_Part_2_softamx_output.p", "wb"))

TRAIN_RESULTS, TEST_RESULTS, TEST_CONFUSION = pickle.load(
    open("Problem_2_Part_2_softamx_output.p", "rb"))

plot_data([list(range(0, STAT_SET['num_epochs'])), list(range(0, STAT_SET['num_epochs']))],
          [TRAIN_RESULTS[:, 0], TEST_RESULTS[:, 0]], ['Train', 'Test'], 'MPCA')

plot_data([list(range(0, STAT_SET['num_epochs'])), list(range(0, STAT_SET['num_epochs']))],
          [TRAIN_RESULTS[:, 1], TEST_RESULTS[:, 1]], ['Train', 'Test'], 'Loss')

print("Maximum testing MPCA: ", np.max(TEST_RESULTS[:, 0]))
print(TEST_CONFUSION)
