import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import pickle
from torch.autograd import Variable

PATH_TO_HOMEWORK_REPO = '/home/rakshit/Documents/DL course'

TRANSFORM = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

BATCH_SIZE = 5

TRAINSET = torchvision.datasets.CIFAR10(root=PATH_TO_HOMEWORK_REPO+'/Data',
                                        train=True, download=True, transform=TRANSFORM)
TRAINLOADER = torch.utils.data.DataLoader(TRAINSET,
                                          batch_size=BATCH_SIZE, shuffle=True, num_workers=2)

TESTSET = torchvision.datasets.CIFAR10(root=PATH_TO_HOMEWORK_REPO+'/Data',
                                       train=False, download=True, transform=TRANSFORM)
TESTLOADER = torch.utils.data.DataLoader(TESTSET,
                                         batch_size=BATCH_SIZE, shuffle=False, num_workers=2)

CLASSES = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

def imshow(img):
    img = 0.5*img + 0.5
    temp = img.numpy()
    plt.imshow(np.transpose(temp, (1, 2, 0)))

DATAITER = iter(TRAINLOADER)
images, labels = DATAITER.next()

imshow(torchvision.utils.make_grid(images))

print(' '.join('%5s' % CLASSES[labels[j]] for j in range(BATCH_SIZE)))
plt.show()

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, 7, 1, (3, 3))
        self.bn1 = nn.BatchNorm2d(64)
        self.pool1 = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(64, 128, 3, 1, (1, 1))
        self.bn2 = nn.BatchNorm2d(128)
        self.conv3 = nn.Conv2d(128, 128, 3, 1, (1, 1))
        self.bn3 = nn.BatchNorm2d(128)
        #Global average pooling
        self.conv4 = nn.Conv2d(128, 10, 1, 1)
        self.avgpool = nn.AvgPool2d(16, 16)
        self.softmax = nn.Softmax()

    def forward(self, x):
        #print("Input shape:", x.data.shape)
        x = F.relu(self.bn1(self.conv1(x)))
        #print("RELU + Conv1:", x.data.shape)
        x = self.pool1(x)
        #print("Max Pool:", x.data.shape)
        x = F.relu(self.bn2(self.conv2(x)))
        #print("RELU + Conv2:", x.data.shape)
        x = F.relu(self.bn3(self.conv3(x)))
        #print("RELU + conv3:", x.data.shape)
        # Global average pooling
        x = self.conv4(x)
        #print("Global Conv4:", x.data.shape)
        x = self.avgpool(x)
        #print("Global average:", x.data.shape)
        x = x.view(-1, 1*1*10)
        #print("Flatten", x.data.shape)
        x = self.softmax(x)
        #print("Softmax:", x.data.shape)
        return x

NET = Net()
NET.cuda()

CRITERION = nn.CrossEntropyLoss()
OPTIMIZER = optim.SGD(NET.parameters(),
                      lr=0.0005, momentum=0.9, dampening=0, weight_decay=0.00009, nesterov=True)

TRAINLOSS = []
EPOCHS = []
for epoch in range(50):

    running_loss = 0.0

    for i, data in enumerate(TRAINLOADER, 0):
        inputs, labels = data
        inputs, labels = Variable(inputs.cuda()), Variable(labels.cuda())
        OPTIMIZER.zero_grad()
        outputs = NET(inputs)
        loss = CRITERION(outputs, labels)
        loss.backward()
        OPTIMIZER.step()

        running_loss += loss.data[0]
        if i%2000 == 1999:
            TRAINLOSS.append(running_loss/ 2000)
            EPOCHS.append(epoch + 1)

            print('[%d, %5d] Loss: %.3f' %(epoch + 1, i + 1, running_loss / 2000))
            running_loss = 0.0

print('Finished Training')

fig = plt.figure()
plt.plot(EPOCHS, TRAINLOSS)
plt.ylabel('Loss')
plt.xlabel('Epochs')
plt.show()

DATAITER = iter(TESTLOADER)
images, labels = DATAITER.next()

imshow(torchvision.utils.make_grid(images))
print('Ground Truth: ', ' '.join('%5s' % CLASSES[labels[j]] for j in range(5)))
plt.show()

NET.eval()

CLASS_CORRECT = list(0. for i in range(10))
CLASS_TOTAL = list(0. for i in range(10))
for data in TESTLOADER:
    images, labels = data
    outputs = NET(Variable(images.cuda())).cpu()
    _, predicted = torch.max(outputs.data, 1)
    c = (predicted == labels).squeeze()
    for i in range(BATCH_SIZE):
        label = labels[i]
        CLASS_CORRECT[label] += c[i]
        CLASS_TOTAL[label] += 1

for i in range(10):
    print('Accuracy of %5s : %2d %%' % (
        CLASSES[i], 100 * CLASS_CORRECT[i] / CLASS_TOTAL[i]))

CONV1_WEIGHTS = NET.state_dict()['conv1.weight'].cpu().numpy()
print(CONV1_WEIGHTS.shape)

W_LIST = []
for i in range(0, CONV1_WEIGHTS.shape[0]):
    temp = CONV1_WEIGHTS[i, :, :, :]
    temp = np.swapaxes(temp, 2, 0)
    temp = np.swapaxes(temp, 0, 1)
    temp = temp - np.min(temp)
    temp = temp/np.max(temp)
    W_LIST.append(temp)

f, PLT_AXES = plt.subplots(4, 16)
for i in range(0,4):
    for j in range(0, 16):
        PLT_AXES[i, j].imshow(W_LIST[16*i + j])
        PLT_AXES[i, j].set_title('Filter_' + str(16*i + j))
f.subplots_adjust(hspace=0.3)
plt.show()

pickle.dump([EPOCHS, TRAINLOSS, CLASS_CORRECT, CLASS_TOTAL], open('HW2_Problem_3_Part_2_DATA.p', 'wb'))
