"""This function solves Problem 2 of HW1"""

import copy as cp
import sys

import matplotlib.pyplot as plt
import numpy as np

PATH_TO_HOMEWORK_REPO = '/home/rakshit/Documents/DL course'
sys.path.insert(0, PATH_TO_HOMEWORK_REPO)

from My_NN_package.My_Neural_Network import Neural_Network

def read_data(file_name):
    """This function reads numeric data from a text file and returns it as a numpy array."""

    with open(PATH_TO_HOMEWORK_REPO + file_name) as fid:
        textdata = fid.read().splitlines()

    temp = np.zeros([len(textdata), 3], dtype="float")

    for i in range(0, len(textdata)):
        temp[i, :] = textdata[i].split()

    print(fid.closed)
    return temp

def plot_data(line_x_list, line_y_list, label_list, title_str):
    """Plotting functions for training and testing loss and mpca"""
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.title(title_str)
    for i in range(0, len(line_y_list)):
        ax.plot(line_x_list[i], line_y_list[i], label=label_list[i])
    colormap = plt.cm.brg #nipy_spectral, Set1,Paired
    colors = [colormap(i) for i in np.linspace(0, 1, len(ax.lines))]
    for i, j in enumerate(ax.lines):
        j.set_color(colors[i])
    ax.legend(loc=0)
    plt.show(block=False)

if __name__ == "__main__":
    TRAINDATA = read_data('/Data/iris-train.txt')
    TESTDATA = read_data('/Data/iris-test.txt')

    print("The size of input data", TRAINDATA.shape)
    print("The size of Target data", TESTDATA.shape)

    MEAN_TRAIN = np.mean(TRAINDATA[:, 1:], 0)
     # Part 1 - ReLU
    STAT_SET1 = {} # Define an empty dictionary
    STAT_SET1['network_inputs'] = TRAINDATA[:, 1:] - MEAN_TRAIN
    STAT_SET1['network_targets'] = TRAINDATA[:, 0:1]
    STAT_SET1['test_inputs'] = TESTDATA[:, 1:] - MEAN_TRAIN
    STAT_SET1['test_targets'] = TESTDATA[:, 0:1]
    STAT_SET1['num_hidden_layers'] = 1
    STAT_SET1['num_hidden_nodes'] = [10,]
    STAT_SET1['list_activation_functions'] = ['relu']
    STAT_SET1['batch_size'] = 10
    STAT_SET1['num_epochs'] = 500
    STAT_SET1['learning_rate'] = [1.*1e-5, 50.*1e-5]
    STAT_SET1['learning_rate_function'] = 'gaussian'
    STAT_SET1['reg_rate'] = 0.009
    STAT_SET1['momentum_function'] = 'nesterov'
    STAT_SET1['momentum'] = 0.9

    NET1 = Neural_Network()
    NET1.initialize_network(STAT_SET1)
    TRAIN_RESULTS, TEST_RESULTS, TEST_CONFUSION = NET1.Train(0)
    NET1.plot_decision_boundary()

    PLOT_X = [range(0, STAT_SET1['num_epochs']), range(0, STAT_SET1['num_epochs'])]
    PLOT_Y = [TRAIN_RESULTS[:, 0], TEST_RESULTS[:, 0]]
    plot_data(PLOT_X, PLOT_Y, ['Train', 'Test'], 'MPCA')
    PLOT_Y = [TRAIN_RESULTS[:, 1], TEST_RESULTS[:, 1]]
    plot_data(PLOT_X, PLOT_Y, ['Train', 'Test'], 'Loss')

    print("Confusion matrix: ", TEST_CONFUSION)
    print("Max MPCA: ", np.max(TEST_RESULTS[:, 0]))
    print("Least Loss: ", np.min(TEST_RESULTS[:, 1]))
    plt.show()

    # Part 2 - Dropout
    PLOT_X = []
    PLOT_Y = []

    for i in range(0, 20):
        STAT_SET2 = {} # Define an empty dictionary
        STAT_SET2['network_inputs'] = TRAINDATA[:, 1:] - MEAN_TRAIN
        STAT_SET2['network_targets'] = TRAINDATA[:, 0:1]
        STAT_SET2['test_inputs'] = TESTDATA[:, 1:] - MEAN_TRAIN
        STAT_SET2['test_targets'] = TESTDATA[:, 0:1]
        STAT_SET2['num_hidden_layers'] = 2
        STAT_SET2['num_hidden_nodes'] = [10, i+1]
        STAT_SET2['list_activation_functions'] = ['sigmoid', 'sigmoid']
        STAT_SET2['batch_size'] = 5
        STAT_SET2['num_epochs'] = 500
        STAT_SET2['learning_rate'] = [1.*1e-5, 50.*1e-5]
        STAT_SET2['learning_rate_function'] = 'gaussian'
        STAT_SET2['reg_rate'] = 0.009
        STAT_SET2['momentum_function'] = 'nesterov'
        STAT_SET2['momentum'] = 0.9
        STAT_SET2['dropout'] = True
        STAT_SET2['dropout_layer_idx'] = [1,]

        STAT_SET3 = cp.deepcopy(STAT_SET2)
        STAT_SET4 = cp.deepcopy(STAT_SET2)
        STAT_SET2['dropout_rate'] = 0
        STAT_SET3['dropout_rate'] = 0.2
        STAT_SET4['dropout_rate'] = 0.5

        NET2 = Neural_Network()
        NET3 = Neural_Network()
        NET4 = Neural_Network()

        NET2.initialize_network(STAT_SET2)
        NET3.initialize_network(STAT_SET3)
        NET4.initialize_network(STAT_SET4)

        TRAIN_RESULTS_2, TEST_RESULTS_2, TEST_CONFUSION_2 = NET2.Train(0)
        TRAIN_RESULTS_3, TEST_RESULTS_3, TEST_CONFUSION_3 = NET3.Train(0)
        TRAIN_RESULTS_4, TEST_RESULTS_4, TEST_CONFUSION_4 = NET4.Train(0)

        #PLOT_X = [range(0, STAT_SET2['num_epochs']), range(0, STAT_SET3['num_epochs']), range(0, STAT_SET4['num_epochs'])]
        #PLOT_Y = [TEST_RESULTS_2[:, 0], TEST_RESULTS_3[:, 0], TEST_RESULTS_4[:, 0]]
        #PLOT_LABELS = ['DO = 0', 'DO = 0.2', 'DO = 0.5']
        #plot_data(PLOT_X, PLOT_Y, PLOT_LABELS, 'MPCA - Dropout')

        #PLOT_Y = [TEST_RESULTS_2[:, 1], TEST_RESULTS_3[:, 1], TEST_RESULTS_4[:, 1]]
        #plot_data(PLOT_X, PLOT_Y, PLOT_LABELS, 'Loss - Dropout')
        #plt.show()
        PLOT_X.append(np.array([i + 1, i + 1, i + 1]))
        PLOT_Y.append(np.array([np.max(TRAIN_RESULTS_2[:, 0]), np.max(TRAIN_RESULTS_3[:, 0]), np.max(TRAIN_RESULTS_4[:, 0])]))
    PLOT_X = np.squeeze(np.asarray(PLOT_X))
    PLOT_Y = np.squeeze(np.asarray(PLOT_Y))
    PLOT_LABELS = ['DO = 0', 'DO = 0.2', 'DO = 0.5']
    plot_data([PLOT_X[:, 0], PLOT_X[:, 1], PLOT_X[:, 2]], [PLOT_Y[:, 0], PLOT_Y[:, 1], PLOT_Y[:, 2]], PLOT_LABELS, 'Training MPCA')
    plt.show()
