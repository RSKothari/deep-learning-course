\documentclass[11pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format

\usepackage[usenames, dvipsnames]{color}
\usepackage{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   		% ... or a4paper or a5paper or ... 
\usepackage[parfill]{parskip}    			
\usepackage{graphicx}				% Use pdf, png, jpg, or epsÂ§ with pdflatex; use eps in DVI mode
\graphicspath{ {Figures/} }										
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{mathtools}
\usepackage{enumerate}
\usepackage{tikz}
\usepackage{subcaption}
\definecolor{myred1}{RGB}{255, 0, 0}
\definecolor{myyellow1}{RGB}{255, 255, 219}
\definecolor{mygreen1}{RGB}{0, 255, 0}
\definecolor{myblue1}{RGB}{0, 0, 255}
\usepackage{placeins}

 \geometry{
 a4paper,
 tmargin=0.2in,
 lmargin=0.5in,
 rmargin=0.5in,
 bmargin=0.2in
 }


\title{Homework 2 \\ IMGS 789 Deep Learning for Vision Fall 2017}
\author{Rakshit Kothari - \texttt{rsk3900@rit.edu}} %uncomment and put your name in!!
\date{}					

\begin{document}

\maketitle

\textbf{Due: October 6, 2017}

\section*{Instructions}

Your homework submission must cite any references used (including articles, books, code, websites, and personal communications).  All solutions must be written in your own words, and you must program the algorithms yourself. \textbf{If you do work with others, you must list the people you worked with.} Submit your solutions as a PDF to the Dropbox Folder on MyCourses.

Your homework solution must be prepared in \LaTeX \, and output to PDF format. I suggest using \url{http://overleaf.com} or BaKoMa \TeX \,  to create your document. Overleaf is free and can be accessed online.

Your programs must be written in  Python. The relevant code to the problem should be in the PDF you turn in. If a problem involves programming, then the code should be shown as part of the solution to that problem. One easy way to do this in \LaTeX \, is to use the verbatim environment, i.e., \textbackslash begin\{verbatim\} YOUR CODE \textbackslash end\{verbatim\}

If you have forgotten your linear algebra, you may find  \textit{The Matrix Cookbook} useful, which can be readily found online. You may wish to use the program \textit{MathType}, which can easily export equations to AMS \LaTeX \, so that you don't have to write the equations in \LaTeX \, directly: \url{http://www.dessci.com/en/products/mathtype/}

\sloppy
\textbf{If told to implement an algorithm, don't use a toolbox, or you will receive no credit.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Problem 1 - Multi-Layer Perceptrons (MLPs)}

In this problem we create a MLP from scratch. Do not use a toolbox. The model we will use has a single hidden layer that has $m$ units. The output layer will be a softmax function and the hidden layer's activation function will vary. This model is given by:
\[
\begin{gathered}
  P\left( {C = k|{\mathbf{x}}} \right) = \frac{{\exp \left( {{\mathbf{w}}_k^T {\mathbf{h}} + b_k } \right)}}
{{\sum\nolimits_j {\exp \left( {{\mathbf{w}}_j^T {\mathbf{h}} + b_j } \right)} }} \\ 
  {\mathbf{h}} = \sigma \left( {{\mathbf{Ux}} + {\mathbf{d}}} \right) \\ 
\end{gathered} 
\]
where $P\left( {C = k|{\mathbf{x}}} \right)$ is the probability of input vector $\mathbf{x} \in \mathbb{R}^d$ being category $k$, $b_k \in \mathbb{R}$ is the bias for the output layer, $\mathbf{w}_k \in \mathbb{R}^m$ is a weight vector for unit $k$ in the output layer, $\mathbf{U} \in \mathbb{R}^{m \times d}$ has the weights for the hidden layer, $\mathbf{d} \in \mathbb{R}^m$ contains the biases in the hidden layer, and $\sigma \left( \cdot \right)$ is the hidden layer activation function.

Because we are using the softmax function as the output layer, we will use cross-entropy loss to optimize the model:
\[
L =  - \sum\limits_{n = 1}^N {\sum\limits_{k = 1}^K {t_k \left( n \right)\log } } P\left( {C = k|{\mathbf{x}}_n } \right),
\]
where $N$ is the total number of training instances, $K$ is the maximum number of classes, and $t_k \left(n \right)$ is equal to one if the correct label for instance $n$ is index $k$ and otherwise zero. Note that the activation function is applied element-wise.

\subsection*{Part 1 - Deriving the Learning Rules (15 points)}

For part 1, you will derive the parameter update rules to use with gradient descent.

\subsubsection*{Output Layer}

a) Compute $\frac{\partial }
{{ \partial{\mathbf{w}}_k }}L$. Show your steps and simplify.

\textbf{Solution:} \\
\[
\frac{\partial }
{{ \partial {\mathbf{w}}_k }}L = \sum_{n=1}^{N}h^T(P(C=k|x_n) - t_k(n))
\]

\textbf{Proof}:
The softmax cross entropy loss is given by:
\begin{equation}
L = -\sum_{n=1}^{N} \sum_{k=1}^{K} t_k(n).log(P)
\end{equation}
Where, $P$ is given by:
\begin{equation}
P\left( {C = k|{\mathbf{x}}} \right) = \frac{{\exp \left( {{\mathbf{w}}_k^T {\mathbf{h}} + b_k } \right)}}
{{\sum\nolimits_j {\exp \left( {{\mathbf{w}}_j^T {\mathbf{h}} + b_j } \right)} }} \\ 
\end{equation}
Expanding $P$ and placing it in the $L$ formula, we get:
\begin{equation}
\begin{aligned}
\frac{\delta L}{\delta \mathbf{w}_k}=-1\frac{1}{\delta \mathbf{w}_k} \sum_{n=1}^{N} \sum_{k=1}^{K} t_k(n) \big[ \log(\exp(\mathbf{w}^T_kh + b_k)) - \log(\sum_{j=1}^{K}\exp(\mathbf{w}^T_jh+b_j) \big]\\
\frac{\delta L}{\delta \mathbf{w}_k}=-1\frac{1}{\delta \mathbf{w}_k} \sum_{n=1}^{N} \sum_{k=1}^{K} \big[ t_k(n)\log(\exp(\mathbf{w}^T_kh + b_k)) - t_k(n)\log(\sum_{j=1}^{K}\exp(\mathbf{w}^T_jh+b_j) \big]\\
\frac{\delta L}{\delta \mathbf{w}_k}=-1 \sum_{n=1}^{N} \sum_{k=1}^{K} \big[ t_k(n)h - \frac{1}{\delta \mathbf{w}_k}t_k(n)\log(\sum_{j=1}^{K}\exp(\mathbf{w}^T_jh+b_j) \big]\\
\frac{\delta L}{\delta \mathbf{w}_k}=-1 \sum_{n=1}^{N} \sum_{k=1}^{K} \big[ t_k(n)h - \frac{\textcolor{myred1}{\frac{1}{\delta \mathbf{w}_k}t_k(n)\sum_{j=1}^{K}\exp(\mathbf{w}^T_kh+b_j}}{\sum_{j=1}^{K}\exp(\mathbf{w}^T_jh+b_j}  \big]\\
\end{aligned}
\end{equation}

Taking out a part of the above equation and simplifying it first, we get the following:
\begin{equation}
\begin{gathered}
\frac{1}{\delta \mathbf{w}_k}t_k(n)\sum_{j=1}^{K}\exp(\mathbf{w}^T_jh+b_j) 
= \frac{1}{\delta \mathbf{w}_k}\sum_{j=1}^{K}t_k(n)\exp(\mathbf{w}^T_jh+b_j)
= h^T\sum_{j=1}^{K}t_k(n)\exp(\mathbf{w}^T_kh+b_j)
\end{gathered}
\end{equation}
Since $t_k(n)$ is a 1-hot vector, the above summation would give us a value for $j=k$. Hence, the above equation can be written as:
\[
\frac{1}{\delta \mathbf{w}_k}t_k(n)\sum_{j=1}^{K}\exp(\mathbf{w}^T_kh+b_j) = h^T\exp(\mathbf{w}^T_kh+b_k)
\]
Replacing the value in the original equation:
\begin{equation}
\begin{aligned}
\frac{\delta L}{\delta \mathbf{w}_k}=-1 \sum_{n=1}^{N} \sum_{k=1}^{K} \big[ h^Tt_k(n) - \frac{h^T\exp(\mathbf{w}^T_kh+b_k)}{\sum_{j=1}^{K}\exp(\mathbf{w}^T_jh+b_j}  \big]\\
\frac{\delta L}{\delta \mathbf{w}_k}=-1 \sum_{n=1}^{N} \sum_{k=1}^{K} \big[ h^Tt_k(n) - h^TP\left( {C = k|{\mathbf{x}}} \right)  \big]\\
\frac{\delta L}{\delta \mathbf{w}_k}=-\sum_{n=1}^{N} \sum_{k=1}^{K} h^T\big[ t_k(n) - P\left( {C = k|{\mathbf{x}}} \right)  \big]
\end{aligned}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%
b) Compute $\frac{\partial }
{\partial {b_k }}L$. Show your steps and simplify.

\textbf{Solution:} \\
\begin{equation}
\frac{\partial }
{\partial {{b}_k }}L = \sum_{n=1}^{N} \sum_{k=1}^{K}P(C = k|x_n) - t_k(n)
\end{equation}
Similar to the previous derivation, we proceed by taking the Loss function and transfering the derivative inside the summation.

\begin{equation}
\begin{aligned}
\frac{\delta L}{\delta b_k} = \frac{-1}{\delta b_k}\sum_{n=1}^{N} \sum_{k=1}^{K}\big[t_k(n)(\mathbf{w}_k^Th + b_k) - t_k(n)\log(\sum_j \exp(\mathbf{w}_j^Th + b_j)) \big]\\
\frac{\delta L}{\delta b_k} = -\sum_{n=1}^{N} \sum_{k=1}^{K} \big[t_k(n) - \frac{\sum_j t_k(n)\exp(\mathbf{w}_j^Th + b_j)}{\sum_j \exp(\mathbf{w}_j^Th + b_j)}\big]
\end{aligned}
\end{equation}

We know that $t_k(n)$ is essentially a 1-hot vector $t$, where the index at which the correct class exists is $1$, while the rest are $0$. That is, the above equation reduces to the following formula:

\begin{equation}
\begin{aligned}
\frac{\delta L}{\delta b_k} = -\sum_{n=1}^{N} \sum_{k=1}^{K} \big[t_k(n) - \frac{\exp(\mathbf{w}_k^Th + b_k)}{\sum_j \exp(\mathbf{w}_j^Th + b_j)}\big]\\
\frac{\delta L}{\delta b_k} = -\sum_{n=1}^{N} \sum_{k=1}^{K} \big[t_k(n) - P(C=k|x_n)\big]\\
\frac{\delta L}{\delta b_k} = \sum_{n=1}^{N} \sum_{k=1}^{K} \big[P(C=k|x_n) - t_k(n)\big]
\end{aligned}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{ReLU Hidden Layer}
If we make the hidden layer activation a rectified linear unit (ReLU) then it is given by
\[
\sigma \left( {\mathbf{a}} \right) = \operatorname{ReLu} \left( {\mathbf{a}} \right) = \max \left( {{\mathbf{a}},0} \right).
\]
c) Compute $\frac{\partial }
{{\partial {\mathbf{U}} }}L$ for ReLU. Show your steps and simplify.

\textbf{Solution:} \\
\begin{equation}
\frac{\partial }
{{\partial {\mathbf{U}} }}L = \frac{\delta L}{\delta h} \frac{\delta h}{\delta \mathbf{U}}
\end{equation}
Following the derivation for $\frac{\delta L}{\delta \mathbf{w}_k}$, it is easy to arrive at the following equation:
\begin{equation}
\begin{aligned}
\frac{\delta L}{\delta h} = -\sum_{n=1}^{N}\sum_{k=1}^{K} \big [ t_k(n) - P(C = k|x_n)\big ]\textbf{w}_k\\
h = \sigma (\textbf{U} x + d)\\
\frac{\delta h}{\delta \mathbf{U}} = \tilde{\sigma}(\mathbf{U}x + d)\frac{\delta(\mathbf{U}x + d)}{\delta \mathbf{U}} = x^T\tilde{\sigma}(\mathbf{U}x + d)\\
\end{aligned}
\end{equation}

The ReLU function $\sigma$ and it's derivative $\tilde{\sigma}$ can be given as follows:
\begin{equation}
    \sigma(x)= 
\begin{cases}
    x, 				&\text{if } x> 0\\
    0,              	&\text{otherwise}
\end{cases}
\end{equation}
\begin{equation}
    \tilde{\sigma}(x)= 
\begin{cases}
    1, 				&\text{if } x> 0\\
    0,              	&\text{otherwise}
\end{cases}
\end{equation}

Combining the two different equations found, we derive that:
\begin{equation}
\begin{aligned}
\frac{\delta L}{\delta \textbf{U}} = -\sum_{n=1}^{N}x_n^T \bigg(\sum_{k=1}^{K} \big [ t_k(n) - P(C = k|x_n)\big ]\textbf{w}_k \bigg) \tilde{\sigma}(\mathbf{U}x + d)
\end{aligned}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%
d) Compute $\frac{\partial }
{\partial \mathbf{d }}L$ for ReLU. Show your steps and simplify.

\textbf{Solution:} \\
This formula can easily be derived from equation 10 and I will directly write the answer and let the reader substitute the variables in the above equation. It's a straightforward replacement of the derivating element.
\[
\frac{\delta L}{\delta d} = -\sum_{n=1}^{N} \bigg(\sum_{k=1}^{K} \big [ t_k(n) - P(C = k|x_n)\big ]\textbf{w}_k \bigg) \tilde{\sigma}(\mathbf{U}x_n + d)
\]
%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Custom Hidden Layer}
Now, let's make the hidden layer activation function SoftPlus, i.e.,
\[
\sigma \left( {\mathbf{a}} \right) = \ln \left( {1 + \exp \left( {\mathbf{a}} \right)} \right)
\]
e) Compute $\frac{\partial }
{{\partial {\mathbf{U}} }}L$ for SoftPlus. Show your steps and simplify.

\textbf{Solution:} \\
\begin{equation}
\begin{aligned}
\frac{\partial }
{{\partial {\mathbf{U}} }}L = \frac{\delta L}{\delta h} \frac{\delta h}{\delta \textbf{U}}\\
\sigma(a) = \log(1 + \exp(a))\\
\tilde{\sigma}(a) = \frac{\exp(a)}{1 + \exp(a)} = \frac{1}{1 + \exp(-a)}\\
\frac{\delta L}{\delta \textbf{U}} = -\sum_{n=1}^{N}x_n^T \bigg(\sum_{k=1}^{K} \big [ t_k(n) - P(C = k|x_n)\big ]\textbf{w}_k \bigg) \tilde{\sigma}(\mathbf{U}x_n + d)\\
\frac{\delta L}{\delta \textbf{U}} = -\sum_{n=1}^{N}x_n^T \bigg(\sum_{k=1}^{K} \big [ t_k(n) - P(C = k|x_n)\big ]\textbf{w}_k \bigg) \frac{1}{1 + \exp(-\textbf{U}x_n - d)}
\end{aligned}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%
f) Compute $\frac{\partial }
{\partial \mathbf{d }}L$ for SoftPlus. Show your steps and simplify.

\textbf{Solution:} \\
\begin{equation}
\frac{\delta L}{\delta \textbf{d}} = -\sum_{n=1}^{N} \bigg(\sum_{k=1}^{K} \big [ t_k(n) - P(C = k|x_n)\big ]\textbf{w}_k \bigg) \frac{1}{1 + \exp(-\textbf{U}x_n - d)}
\end{equation}

\subsection*{Part 2 - Implementing an MLP with ReLU Hidden Units (20 points)}

For this problem, you will use the 2-dimensional Iris dataset. Download \texttt{iris-train.txt} and \texttt{iris-test.txt} from MyCourses. Each row is one data instance. The first column is the label (1, 2 or 3) and the next two columns are features.

Using the update rules you derived, implement the neural network using ReLU units with mini-batches and momentum. You may use NumPy, but do not use a neural network toolbox. Remember to compute the mean of the training data and subtract it from the input to the next for both training and testing.

Optimize the number of hidden units and the hyper-parameters to perform well on the data. Report the number of units and other hyper-parameters. Give the mean-per-class accuracy and plot a decision boundary. 

\textbf{Solution:} \\

\begin{enumerate}
\item Maximum testing MPCA: 89.305$\%$
\item Number of hidden units: 10
\item Learning rate: 1.*1e-5 $\rightarrow$ 50.*1e-5 [GAUSSIAN]
\item Momentum: Nesterov
\item Momentum: 0.9
\item Weight Decay: 0.009
\end{enumerate}

\begin{figure}[!tbp]
	\centering
	\begin{subfigure}[t]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{500Epoch_Problem_1_Part_2_CP.png}
		\caption{1 Hidden Layer NN Decision Boundary for ReLU Hidden Units}
		\label{fig:fig_1}
	\end{subfigure}
~
	\begin{subfigure}[t]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{500Epoch_Problem_1_Part_2.png}
		\caption{Train and Test Loss as a function of Epochs}
		\label{fig:fig_2}
	\end{subfigure}
	\caption{Output from NN with ReLU hidden units}
\end{figure}

\begin{figure}[!tbp]
\centering
\includegraphics[width=0.7\textwidth]{500Epoch_Problem_1_Part_2_MCPA.png}
\caption{Train and Test MPCA as a function of Epochs}
\label{fig:fig_3}
\end{figure}
\FloatBarrier

\subsection*{Part 3 - Dropout (5 points)}
Implement dropout in the hidden layer of the network. Draw a plot with three lines of the mean-per-class test accuracy as a function of the number of hidden units in the network from 1 to 10. The different lines will correspond to dropout rates of 0, 0.2, and 0.5. Note that  a dropout rate of 0 corresponds to not using dropout at all.

Evaluate performance on the Iris dataset. Make sure to label the plot and provide a legend.

\textbf{Solution:} \\

Please look at figure \ref{fig:fig_4}

For a small dataset like this one, it makes sense that dropout is reducing the performance of the network.

\begin{figure}[!tbp]
\centering
\includegraphics[width=0.7\textwidth]{Dropouts.png}
\caption{Dropout performance as a function of Epochs. XLABEL $\rightarrow$ Epochs. YLABEL $\rightarrow$ Max Testing MPCA}
\label{fig:fig_4}
\end{figure}

\FloatBarrier
%----------------------
\section*{Problem 2 - Transfer Learning with a Pre-Trained CNN}

For this problem you must use a toolbox. We will do image classification using the Oxford Pet Dataset. The dataset consists of 37 categories with about 200 images in each of them. To make your life easier, I parsed the files for you and split them into train (\texttt{PetsTrain.mat}) and test (\texttt{PetsTest.mat}) sets. The file names are in the \texttt{files} field and the labels are in the \texttt{label} field.

\subsection*{Part 1 - Using Pre-Trained Deep CNN (5 points)}
For this problem you will use a CNN that has been trained on ImageNet. Choose the pre-trained model of your choice (e.g., VGG-16, VGG-19, ResNet-18, ResNet-152, etc.). Run it on \texttt{peppers.png}. Output the top-3 predicted categories and the probabilities for both models.

Make sure to list the deep CNN model you used. Make sure to pre-process the input image appropriately, e.g., resize the image to the appropriate size,  subtract the image mean that was computed on ImageNet, etc.

\textbf{Solution:} \\

I tried running multiple images on VGG16 and RESNET152. The most important thing that I learned was to set a network into \textit{evaluation} mode. This applies the running memory of Batch-Normalization on testing data and removes Dropouts for testing purposes without affecting the output in anyway. I also found that the classification was heavily affected when we change the color channels. That is, if the peppers appear blue, then it does not classify correctly.

I also observed that for VGG16, if I don't normalize the image, it still gives the correct classification, albeit the classified probability is slighty reduced.

\begin{enumerate}
\item VGG16 (with normalization): P = 0.15087675423, bell pepper
\item VGG16 (without normalization): P = 0.128408933113, bell pepper
\item RESNET152 (with normalization): P = 0.171995448197, bell pepper
\item RESNET152 (without normalization): P = 0.13.555663531, bell pepper
\end{enumerate}
%%
\subsection*{Part 2 - Deep Transfer Learning (25 points)}
Rather than using the final `softmax' layer of the CNN as output to make predictions, instead we will use the CNN as a feature extractor to classify the Pets dataset. For each image, grab features from the last hidden layer of the neural network, which will be the layer \textbf{before} the 1000-dimensional output layer (around 2000--6000 dimensions). You will need to resize the images to a size compatible with your network (usually $224 \times 224 \times 3$). If your CNN model is using ReLUs, then you should set all negative values to zero. 

After you extract these features for all of the images in the dataset, normalize them to unit length by dividing by the $L_2$ norm. Train a linear classifier of your choice~\footnote{You could use the softmax classifier you implemented for homework 1.} with the training CNN features, and then classify the test CNN features. Report mean-per-class accuracy and discuss the classifier you used. 

\textbf{Solution:}\\

I decided to use my own custom made NN toolbox and generated my own Softmax classifier. The features extracted from the VGG16 network are of the size 4096. We removed the last FCN (Fully Connected Layer). Surprinsgly, I did not find a softmax operation in the output of VGG16. My first training attempt resulted in a very high training accuracy but a low testing accuracy. Upon fine tuning my network,

\begin{enumerate}
\item Maximum testing MPCA: 68.65246$\%$
\item Number of hidden units: 0
\item Learning rate: 1.*1e-4 $\rightarrow$ 50.*1e-4 [GAUSSIAN]
\item Momentum: Nesterov
\item Momentum: 0.75
\item Weight Decay: 0.09
\end{enumerate}

\begin{figure}[!tbp]
\centering
	\begin{subfigure}[t]{0.45\textwidth}
	\includegraphics[width=\textwidth]{Problem_2_Part_2_Transfer_Learning.png}
	\caption{Training and Testing MPCA}
	\label{fig:fig_5}
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.45\textwidth}
	\includegraphics[width=\textwidth]{Problem_2_Part_2_Transfer_Learning_Loss.png}
	\caption{Training and Testing Loss}
	\label{fig:fig_6}
	\end{subfigure}
\caption{Transfer learning performance}	
\end{figure}
 
 \FloatBarrier
%--------------------------------------------
\section*{Problem 3 - Training a Small CNN}

\section*{Part 1 (30 points)}

For this problem you must use a toolbox. Train a CNN with three hidden convolutional layers that use the ReLU activation function. Use 64 $7\times 7$ filters for the first layer, followed by $2 \times 2$ max pooling (stride of 2). The next two convolutional layers will use 128 $3 \times 3$ filters followed by the ReLU activation function. Prior to the softmax layer, you should have an average pooling layer that pools across the preceding feature map. Do not use a pre-trained CNN.

Train your model using all of the CIFAR-10 training data, and evaluate your trained system on the CIFAR-10 test data.

Visualize all of the $7 \times 7 \times 3$ filters learned by the first convolutional layer as an image array (I suggest an image array with 4 rows and 16 columns), similar to the ones we saw in class. Note that you will need to normalize them by contrast stretching to do this visualization, i.e., subtract the smallest value and then divide by the new largest value.

Display the training loss as a function of epochs. What is the accuracy on the test data? How did you initialize the weights? Discuss your architecture and hyper-parameters.

\textbf{Solution:}\\

\begin{figure}
\includegraphics[width=\textwidth]{Weights.png}
\caption{Weights of the first Conv layer.}
\label{fig:fig_7}
\end{figure}

All weights were intialized randomly. PyTorch has no ready-to-use function for weight initialization. I considered implementing Xaviers Weight initialization, however in the interest of saving time, I did not use it.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{50Epochs_Problem_3_Part_1}
\caption{Training Loss}
\label{fig:fig_8}
\end{figure}

The testing MPCA for this architecture was found to be 58.6 $\%$:

\begin{itemize}
\item Accuracy of plane : 50 $\%$
\item Accuracy of   car : 78 $\%$
\item Accuracy of  bird : 40 $\%$
\item Accuracy of   cat : 38 $\%$
\item Accuracy of  deer : 45 $\%$
\item Accuracy of   dog : 64 $\%$
\item Accuracy of  frog : 70 $\%$
\item Accuracy of horse : 53 $\%$
\item Accuracy of  ship : 78 $\%$
\item Accuracy of truck : 70 $\%$
\end{itemize}
The hyper-parameters used by me were as follows:

\begin{itemize}
\item Learning Rate = 0.0005
\item Momemtum = 0.9
\item Weight Decay = 0.00009
\item Momemtum function = Nesterov
\item Epochs = 50
\item Batch size = 5
\end{itemize}

\FloatBarrier
\section*{Part 2 (20 points)}

Using the same architecture as in part 1, add in batch normalization between each of the hidden layers. Compare the training loss with and without batch normalization as a function of epochs. What is the final test error?

\textbf{Solution:}\\

By adding Batch Norm, I experienced the error reducing and an improvement in performance. Hence, the training time is lower than the previous architecture without Batch Norm. Batch Norm doesn't offer significant reductions for small network sizes but can be a powerful tool for larger networks. The performance of this architecture gave us a 68.5 $\%$ MPCA

\begin{itemize}
\item Accuracy of plane : 83 $\%$
\item Accuracy of   car : 81 $\%$
\item Accuracy of  bird :  0 $\%$
\item Accuracy of   cat : 51 $\%$
\item Accuracy of  deer : 76 $\%$
\item Accuracy of   dog : 78 $\%$
\item Accuracy of  frog : 80 $\%$
\item Accuracy of horse : 77 $\%$
\item Accuracy of  ship : 88 $\%$
\item Accuracy of truck : 71 $\%$
\end{itemize}

Notice that one of the classes was not classified at all. This means the network doesn't have enough complexity to capture the statistics of the dataset.

\begin{figure}
\includegraphics[width=0.8\textwidth]{EffectBN.png}
\caption{Effect of Batch Normalization for 50 Epochs. XLABEL $\rightarrow$ Epochs. YLABEL $\rightarrow$ Loss}
\label{fig:fig_9}
\end{figure}

\FloatBarrier

\section*{Part 3 (10 points)}
Can you do better with a deeper network? Optimize your CNN's architecture to improve performance. You may get significantly better results by using smaller filters for the first convolutional layer. Describe your model's architecture and your design choices. What is your final accuracy?

Note: Your model should perform better than the one in Part 1 and Part 2.

\textbf{Solution:}\\

I observed a very large boost in performance with my modified architecture (MPCA = 84.2 $\%$).
\begin{itemize}
\item Accuracy of plane : 80 $\%$
\item Accuracy of   car : 90 $\%$
\item Accuracy of  bird : 77 $\%$
\item Accuracy of   cat : 76 $\%$
\item Accuracy of  deer : 83 $\%$
\item Accuracy of   dog : 76 $\%$
\item Accuracy of  frog : 90 $\%$
\item Accuracy of horse : 85 $\%$
\item Accuracy of  ship : 92 $\%$
\item Accuracy of truck : 93 $\%$
\end{itemize}

\begin{figure}
\includegraphics[width=0.8\textwidth]{BN_50Epochs_Problem_3_Part_3}
\caption{Performance of my own deeper architecture}
\label{fig:fig_10}
\end{figure}

The new architecture was trained for 50 Epochs but considering the reducing trend in loss, we could train the network further for probable better performance. We can theoretically keep going deeper with diminishing returns. The network should appropriately capture the variations in the dataset. Poor performance in a few classes indicates a low model complexity and the need for adding more layers.

\begin{figure}
\centering
\includegraphics[width = 0.8\linewidth]{Problem_3_ModelFlow_crop}
\caption{My own Model: Flow Diagram}
\label{fig:fig_11}
\end{figure}

\FloatBarrier
%---------------
\section*{Code Appendix}

For all HW codes, please visit my GIT repo at: https://bitbucket.org/RSKothari/deep-learning-course
\end{document}  