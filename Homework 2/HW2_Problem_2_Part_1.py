from __future__ import division, print_function

import pickle
import sys

import matplotlib.pyplot as plt
import numpy as np
import torch
#import torch.nn as nn
#import torch.optim as optim
#from torch.optim import lr_scheduler
from torch.autograd import Variable
from torchvision import models

import cv2

PATH_TO_HOMEWORK_REPO = '/home/rakshit/Documents/DL course'
sys.path.insert(0, PATH_TO_HOMEWORK_REPO)

from visualize import make_dot

# Part 1
MODEL_VGG16 = models.resnet152(pretrained=True)
MODEL_VGG16.double()
MODEL_VGG16.eval()

IMG = cv2.imread('peppers.jpg')
IMG = IMG[:, :, ::-1] #Convert BGR to RGB

# Show the image
plt.figure(0)
plt.imshow(IMG)
plt.show()

IMG = cv2.resize(IMG, (224, 224), interpolation=cv2.INTER_CUBIC)
IMG = IMG.astype(np.float)/255
IMG = (IMG - np.array([0.485, 0.456, 0.406]))/np.array([0.229, 0.224, 0.225])
IMG = np.swapaxes(IMG, 0, 2)
IMG = np.swapaxes(IMG, 1, 2)

IP = torch.from_numpy(IMG).unsqueeze(0)

IP = Variable(IP, requires_grad=False)
OUT = MODEL_VGG16.forward(IP)
#GRAPH = make_dot(OUT)
#GRAPH.view()
OUT = np.squeeze(OUT.data.numpy())

LOC = np.argmax(OUT)
print("Prediction probability: ", OUT[LOC])
fid = open(PATH_TO_HOMEWORK_REPO + '/Data/imagenet1000_clsid_to_human.pkl', 'rb')
IMAGENET_CLASSES = pickle.load(fid)
print(IMAGENET_CLASSES[LOC])
