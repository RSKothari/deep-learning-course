"""This function solves Problem 2 of HW1"""

import sys
import numpy as np
import matplotlib.pyplot as plt
PATH_TO_HOMEWORK_REPO = '/home/rakshit/Documents/DL course'
sys.path.insert(0, PATH_TO_HOMEWORK_REPO)

from My_NN_package import Neural_Network

def read_data(file_name):
    """This function reads numeric data from a text file and returns it as a numpy array."""

    with open(PATH_TO_HOMEWORK_REPO + file_name) as fid:
        textdata = fid.read().splitlines()

    temp = np.zeros([len(textdata), 3], dtype="float")

    for i in range(0, len(textdata)):
        temp[i, :] = textdata[i].split()

    print fid.closed
    return temp

def plot_data(line_x, line_y_list, label_list, title_str):
    """Plotting functions for training and testing loss and mpca"""
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.title(title_str)
    for i in range(0, len(line_y_list)):
        ax.plot(line_x, line_y_list[i], label=label_list[i])
    colormap = plt.cm.brg #nipy_spectral, Set1,Paired
    colors = [colormap(i) for i in np.linspace(0, 1, len(ax.lines))]
    for i, j in enumerate(ax.lines):
        j.set_color(colors[i])
    ax.legend(loc=0)
    plt.show(block=False)

if __name__ == "__main__":
    TRAINDATA = read_data('/Data/iris-train.txt')
    TESTDATA = read_data('/Data/iris-test.txt')

    print "The size of input data", TRAINDATA.shape
    print "The size of Target data", TESTDATA.shape

    MEAN_TRAIN = np.mean(TRAINDATA[:, 1:], 0)

    STAT_SET = {} # Define an empty dictionary
    STAT_SET['network_inputs'] = TRAINDATA[:, 1:] - MEAN_TRAIN
    STAT_SET['network_targets'] = TRAINDATA[:, 0:1]
    STAT_SET['test_inputs'] = TESTDATA[:, 1:] - MEAN_TRAIN
    STAT_SET['test_targets'] = TESTDATA[:, 0:1]
    STAT_SET['num_hidden_layers'] = 1
    STAT_SET['num_hidden_nodes'] = [3,]
    STAT_SET['list_activation_functions'] = ['relu', 'sigmoid', 'sigmoid', 'sigmoid']
    STAT_SET['batch_size'] = 10
    STAT_SET['num_epochs'] = 500
    STAT_SET['learning_rate'] = [1.*1e-4, 50.*1e-4]
    STAT_SET['learning_rate_function'] = 'gaussian'
    STAT_SET['reg_rate'] = 0.009
    STAT_SET['momentum_function'] = 'nesterov'
    STAT_SET['momentum'] = 0.8

    NET = Neural_Network()
    NET.initialize_network(STAT_SET)
    TRAIN_RESULTS, TEST_RESULTS, TEST_CONFUSION = NET.Train(0)
    NET.plot_decision_boundary()

    plot_data(range(0, STAT_SET['num_epochs']), [TRAIN_RESULTS[:, 0], TEST_RESULTS[:, 0]], ['Train', 'Test'], 'MPCA')
    plot_data(range(0, STAT_SET['num_epochs']), [TRAIN_RESULTS[:, 1], TEST_RESULTS[:, 1]], ['Train', 'Test'], 'Loss')
    plt.show()

    print "Confusion matrix: \n", TEST_CONFUSION
    print "Max MPCA: ", np.max(TEST_RESULTS[:, 0])
    print "Least Loss: ", np.min(TEST_RESULTS[:, 1])
    