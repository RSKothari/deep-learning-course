\documentclass[11pt, oneside]{article}                   		
\usepackage[parfill]{parskip}
\usepackage{graphicx}								
\usepackage[usenames, dvipsnames]{color}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{mathtools}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{ulem}
\usepackage{placeins}
\usepackage{float}
\graphicspath{ {Figures/} }
\usepackage{caption}
\usepackage{subcaption}
\usepackage{minted} 
\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage[table,xcdraw]{xcolor}

\usepackage{geometry}
 \geometry{
 a4paper,
 tmargin=0.1in,
 lmargin=0.5in,
 rmargin=0.5in,
 bmargin=0.1in
 }

\definecolor{myred}{rgb}{1.0, 0.0, 0.0}

\title{Homework 1 - Solutions\\ IMGS 789 Deep Learning for Vision Fall 2017}
\author{Rakshit Kothari - \texttt{rsk3900@rit.edu}}

\date{}					

\begin{document}

\maketitle

\textbf{Due: 9:00 PM EDT, September 22, 2017}

\section*{Instructions}

Your homework submission must cite any references used (including articles, books, code, websites, and personal communications).  All solutions must be written in your own words, and you must program the algorithms yourself. \textbf{If you do work with others, you must list the people you worked with.} Submit your solutions as a PDF to the Dropbox Folder on MyCourses.

Your homework solution must be prepared in \LaTeX \, and output to PDF format. I suggest using \url{http://overleaf.com} or BaKoMa \TeX \,  to create your document. Overleaf is free and can be accessed online.

Your programs must be written in Python. The relevant code to the problem should be in the PDF you turn in. If a problem involves programming, then the code should be shown as part of the solution to that problem. One easy way to do this in \LaTeX \, is to use the verbatim environment, i.e., \textbackslash begin\{verbatim\} YOUR CODE \textbackslash end\{verbatim\}

If you have forgotten your linear algebra, you may find  \textit{The Matrix Cookbook} useful, which can be readily found online. You may wish to use the program \textit{MathType}, which can easily export equations to AMS \LaTeX \, so that you don't have to write the equations in \LaTeX \, directly: \url{http://www.dessci.com/en/products/mathtype/}

\sloppy
\textbf{If told to implement an algorithm, don't use a toolbox, or you will receive no credit.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Problem 1 - Softmax Properties}

\subsection*{Part 1 (7 points)}
Recall the softmax function, which is the most common activation function used for the output of a neural network trained to do classification. In a vectorized form, it is given by
\begin{equation*}
\operatorname{softmax} \left( {\mathbf{a}} \right) = \frac{{\exp \left( {\mathbf{a}} \right)}}
{{\sum\nolimits_{j = 1}^K {\exp \left( {a_j } \right)} }},
\end{equation*}
where $\mathbf{a}\in \mathbb{R}^K$. The $\exp$ function in the numerator is applied element-wise and $a_j$ denotes the $j$'th element of $\mathbf{a}$.

Show that the softmax function is invariant to constant offsets to its input, i.e., 
\[
\operatorname{softmax} \left( {{\mathbf{a}} + {c\mathbf{1}}} \right) = \operatorname{softmax} \left( {\mathbf{a}} \right),
\]
where $c\in \mathbb{R}$ is some constant and $\mathbf{1}$ denotes a column vector of 1's.

\textbf{Solution:} \\

Let $f$ define a softmax function as:

\begin{equation}
f(a_i) = \frac{\exp(a_i)}{\sum_{j=1}^{K}\exp(a_j)}
\end{equation}

For every single element $a_i$ of $a$, we have a corresponding output $f(a_i)$. Given a fixed offset, we can expand the equation as such:

\begin{equation} \label{exp1}
\begin{split}
f(a_i + c_i) = \frac{\exp(a_i + c_i)}{\sum_{j=1}^{K}\exp(a_j + c_j)}\\
f(a_i + c_i) = \frac{\exp(a_i) \exp(c_i)}{\sum_{j=1}^{K}\exp(a_j)\exp(c_j)}
\end{split}
\end{equation}

Since $c_j$ is constant for every single iteration of $j$, we can replace $c_j$ by $c$ and $\exp(c_i)$ by $\exp(c)$.

\begin{equation}
f(a_i + c) = \frac{\exp(a_i) \text{\sout{$\exp(c)$}}}{\text{\sout{$\exp(c)$}}\sum_{j=1}^{K}a_j}\\
\end{equation} 

Hence proved, that for every single element $a_i$, $f(a_i + c) = f(a_i)$. However, in the event offsets are variable for every input $a_i$ (unlike the case asked in the question), then $f(a_i+c_i) \neq f(a_i)$

%----------------------------------
\subsection*{Part 2 (3 points)}
In practice, why is the observation that the softmax function is invariant to constant offsets to its input important when implementing it in a neural network?

\textbf{Solution:} \\

This signifies that the softmax function is invariant to constant offset to it's input variables. The biggest numerical advantage is that it constraints the output probabilities with a fixed range (between: [0 $\rightarrow$ 1]). Observe the plot for $e^{-x}$ in figure 1. 

\begin{figure}[H]
\includegraphics[width=0.5\textwidth]{exp_soft.png}
\centering
\end{figure}

Note, that all values $x < 0$ are constrained between 0 and 1, which ensures that the softmax probability outputs do not overshoot. $e^x$ increase astronomically for a moderate magnitude value of $x$. Hence, to avoid all overshooting problems, we subtract the max and ensure the probabilities are within limits. 
%---------------------------------
\section*{Problem 2 - Implementing a Softmax Classifier}

For this problem, you will use the 2-dimensional Iris dataset. Download \texttt{iris-train.txt} and \texttt{iris-test.txt} from MyCourses. Each row is one data instance. The first column is the label (1, 2 or 3) and the next two columns are features.

\subsection*{Part 1 - Implementation \& Evaluation (30 points)}
Recall that a softmax classifier is a shallow one-layer neural network of the form:
\begin{equation*}
P\left( {C = k|{\bf{x}}} \right) = \frac{{\exp \left( {{\bf{w}}_k^T {\bf{x}}} \right)}}
{{\sum\nolimits_{j = 1}^K {\exp \left( {{\bf{w}}_j^T {\bf{x}}} \right)} }}
\end{equation*}
where $\mathbf{x}$ is the vector of inputs, $K$ is the total number of categories, and $\mathbf{w}_k$ is the weight vector for category $k$.

In this problem you will implement a softmax classifier from scratch. \textbf{Do not use a toolbox.} Use the softmax (cross-entropy) loss with $L_2$ weight decay regularization. Your implementation should use stochastic gradient descent with mini-batches and momentum to minimize softmax (cross-entropy) loss of this single layer neural network. To make your implementation fast, do as much as possible using matrix and vector operations. This will allow your code to use your environment's BLAS. Your code should loop over epochs and mini-batches, but do not iterate over individual elements of vectors and matrices. Try to make your code as fast as possible. I suggest using profiling and timing tools to do this.

Train your classifier on the Iris dataset for 1000 epochs. Hand tune the hyperparameters (i.e., learning rate, mini-batch size, momentum rate, and $L_2$ weight decay factor) to achieve the best possible training accuracy. During a training epoch, your code should compute the mean per-class accuracy for the training data and the loss. After each epoch, compute the mean per-class accuracy for the testing data and the loss as well. \textbf{The test data should not be used for updating the weights.}

After you have tuned the hyperparameters, generate two plots next to each other. The one on the left should show the cross-entropy loss during training for both the train and test sets as a function of the number of training epochs. The plot on the right should show the mean per-class accuracy as a function of the number of training epochs on both the train set and the test set. 

What is the best test accuracy your model achieved? What hyperparameters did you use? Would early stopping have helped improve accuracy on the test data?\\

\textbf{Solution:} \\

\textcolor{myred}{I have implemented my own Neural Network toolbox. Attached is the entire toolbox code and the script to effectively import and run it. I have also shared the repo link. To recreate my code, I recommend cloning and pulling the latest commits.}\\

\textbf{Network parameters}:

\begin{itemize}
\item Number of Hidden Layers: 0
\item Number of Epochs: 1000
\item Batch Size: 10
\item Weight Decay: 0.009
\item Momemtum: 0.8
\item Momemtum function: Nesterov
\item Learning rate: 0.01 $\rightarrow$ 0.009 [Gaussian]
\end{itemize}

\begin{figure}[H]
\includegraphics[width=\textwidth, height=5cm]{Loss&MPCA.png}
\centering
\caption{Note that [Green] is the testing loss/MPCA while [Blue] is the training loss}
\end{figure}

\textbf{Results:}

\begin{itemize}
\item Confusion Matrix:
\begin{table}[h]
\centering
\resizebox{0.2\textwidth}{!}{%
\begin{tabular}{|l|l|l|l|}
\hline
CM & \textbf{1} & \textbf{2} & \textbf{3} \\ \hline
\textbf{1} & 20 & 0 & 0 \\ \hline
\textbf{2} & 0 & 11 & 4 \\ \hline
\textbf{3} & 0 & 2 & 14 \\ \hline
\end{tabular}%
}
\caption{Confusion Matrix - Iris dataset. \textbf{Rows}: Ground Truth. \textbf{Columns}: Predicted class}
\label{confusion_mat_1}
\end{table}

\item Max Mean Per Class Accuracy: 88.225
\item Least Loss: 25.7880194 
\end{itemize}

Fortunately, (or unfortunately, in this context), I never observed the train loss to start increasing again. Once, converged to an optimum value, the Loss flattened out. The MPCA, however, keeps fluctuating and hence, it makes sense to stop the code when it comes across the highest MPCA and store the epoch number and parameters as the optimal solution.
%--------------------
\subsection*{Part 2 - Displaying Decision Boundaries (10 points)}

Plot the decision boundaries learned by softmax classifier on the Iris dataset, just like we saw in class. On top of the decision boundaries, generate a scatter plot of the training data. Make sure to label the categories.

\textbf{Solution:} \\

\begin{figure}[H]
\centering
\begin{subfigure}{0.5\textwidth}
	\centering
	\includegraphics[width=\textwidth]{Decision_Boundary.png}
	\caption{NN Decision Boundary for 0 Hidden Layers}
	\label{fig:DB_1}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
	\centering
	\includegraphics[width=\textwidth]{NN_Decision_Boundary_HL4_Epoch_10000.png}
	\caption{NN Decision Boundary for 4 Hidden Layers}
	\label{fig:DB_2}
\end{subfigure}
\caption{Decision Boundaries}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Problem 3 - Classifying Images}

The CIFAR-10 dataset contains 60,000 RGB images from 10 categories. Download it from here: \url{https://www.cs.toronto.edu/~kriz/cifar.html}\\
Read the documentation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection*{Part 1 (10 points)}

Using the first CIFAR-10 training batch file, display the first three images from each of the 10 categories as a $3 \times 10$ image array. The images are stored as rows, and you will need to reshape them into $32 \times 32 \times 3$ images. 

\textbf{Solution:}\\

\begin{figure}[H]
\includegraphics[width=\textwidth]{CIFAR10.png}
\caption{3 images from the first batch of each category in CIFAR-10.}
\label{CIFAR10}
\end{figure}

%-----------
\subsection*{Part 2 (20 points)}
Using the softmax classifier you implemented, train the model on CIFAR-10's training partitions. To do this, you will need to treat each image as a vector. You will need to tweak the hyperparmaters you used earlier. 

Plot the training loss as a function of training epochs. Try to minimize the error as much as possible. What were the best hyperparmeters? Output the final test accuracy and a normalized $10 \times 10$ confusion matrix computed on the test partition. Make sure to label the columns and rows of the confusion matrix.

\textbf{Solution:}\\

\begin{table}[]
\centering
\label{my-label}
\begin{tabular}{|
>{\columncolor[HTML]{C0C0C0}}c |c|c|c|c|c|c|c|c|c|c|}
\hline
\cellcolor[HTML]{343434}{\color[HTML]{FFFFFF} \textbf{CM}} & \cellcolor[HTML]{C0C0C0}\textbf{airplane} & \cellcolor[HTML]{C0C0C0}\textbf{automobile} & \cellcolor[HTML]{C0C0C0}\textbf{bird} & \cellcolor[HTML]{C0C0C0}\textbf{cat} & \cellcolor[HTML]{C0C0C0}\textbf{deer} & \cellcolor[HTML]{C0C0C0}\textbf{dog} & \cellcolor[HTML]{C0C0C0}\textbf{frog} & \cellcolor[HTML]{C0C0C0}\textbf{horse} & \cellcolor[HTML]{C0C0C0}\textbf{ship} & \cellcolor[HTML]{C0C0C0}\textbf{truck} \\ \hline
\textbf{airplane}                                          & 751.                                      & 38.                                         & 0.                                    & 30.                                  & 0.                                    & 0.                                   & 41.                                   & 60.                                    & 79.                                   & 1.                                     \\ \hline
\textbf{automobile}                                        & 191.                                      & 497.                                        & 1.                                    & 70.                                  & 1.                                    & 0.                                   & 68.                                   & 76.                                    & 93.                                   & 3.                                     \\ \hline
\textbf{bird}                                              & 290.                                      & 39.                                         & 21.                                   & 149.                                 & 3.                                    & 6.                                   & 275.                                  & 182.                                   & 35.                                   & 0.                                     \\ \hline
\textbf{cat}                                               & 211.                                      & 65.                                         & 3.                                    & 330.                                 & 2.                                    & 9.                                   & 224.                                  & 118.                                   & 38.                                   & 0.                                     \\ \hline
\textbf{deer}                                              & 190.                                      & 33.                                         & 12.                                   & 128.                                 & 15.                                   & 1.                                   & 373.                                  & 226.                                   & 22.                                   & 0.                                     \\ \hline
\textbf{dog}                                               & 254.                                      & 62.                                         & 1.                                    & 282.                                 & 1.                                    & 11.                                  & 173.                                  & 160.                                   & 55.                                   & 1.                                     \\ \hline
\textbf{frog}                                              & 78.                                       & 37.                                         & 5.                                    & 166.                                 & 5.                                    & 2.                                   & 586.                                  & 93.                                    & 26.                                   & 2.                                     \\ \hline
\textbf{horse}                                             & 193.                                      & 59.                                         & 0.                                    & 99.                                  & 1.                                    & 4.                                   & 91.                                   & 527.                                   & 26.                                   & 0.                                     \\ \hline
\textbf{ship}                                              & 435.                                      & 74.                                         & 0.                                    & 42.                                  & 0.                                    & 1.                                   & 24.                                   & 34.                                    & 389.                                  & 1.                                     \\ \hline
\textbf{truck}                                             & 315.                                      & 301.                                        & 0.                                    & 66.                                  & 0.                                    & 2.                                   & 70.                                   & 122.                                   & 113.                                  & 11.                                    \\ \hline
\end{tabular}
\caption{Confusion Matrix. CIFAR-10}
\end{table}

Hyperparamters used:
\begin{itemize}
\item Number of Hidden Layers: 0
\item Number of Epochs: 100
\item Batch Size: 500
\item Weight Decay: 0.00001
\item Momemtum: 0.8
\item Momemtum function: Nesterov
\item Learning rate: 0.0004$\rightarrow$0.0001 [Gaussian]
\end{itemize}

Final test MPCA for CIFAR-10: 32.5$\%$
%---------------
\section*{Softmax Classifier Code Appendix}

\subsection*{Problem 2 Code}

\begin{minted}{python}
"""This function solves Problem 2 of HW1"""

import os
import numpy as np
import matplotlib.pyplot as plt
from My_Neural_Network import Neural_Network

def read_data(file_name):
    """This function reads numeric data from a text file and returns it as a numpy array."""

    with open(os.getcwd() + file_name) as fid:
        textdata = fid.read().splitlines()

    temp = np.zeros([len(textdata), 3], dtype="float")

    for i in range(0, len(textdata)):
        temp[i, :] = textdata[i].split()

    print fid.closed
    return temp

def plot_data(line_x, line_y_list, label_list, title_str):
    """Plotting functions for training and testing loss and mpca"""
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.title(title_str)
    for i in range(0, len(line_y_list)):
        ax.plot(line_x, line_y_list[i], label=label_list[i])
    colormap = plt.cm.brg #nipy_spectral, Set1,Paired
    colors = [colormap(i) for i in np.linspace(0, 1, len(ax.lines))]
    for i, j in enumerate(ax.lines):
        j.set_color(colors[i])
    ax.legend(loc=0)
    plt.show(block=False)

if __name__ == "__main__":
    TRAINDATA = read_data('/Data/iris-train.txt')
    TESTDATA = read_data('/Data/iris-test.txt')

    print "The size of input data", TRAINDATA.shape
    print "The size of Target data", TESTDATA.shape

    STAT_SET = {} # Define an empty dictionary
    STAT_SET['network_inputs'] = TRAINDATA[:, 1:]
    STAT_SET['network_targets'] = TRAINDATA[:, 0:1]
    STAT_SET['test_inputs'] = TESTDATA[:, 1:]
    STAT_SET['test_targets'] = TESTDATA[:, 0:1]
    STAT_SET['num_hidden_layers'] = 0
    STAT_SET['num_hidden_nodes'] = [0,]
    STAT_SET['list_activation_functions'] = ['sigmoid', 'sigmoid', 'sigmoid', 'sigmoid']
    STAT_SET['batch_size'] = 10
    STAT_SET['num_epochs'] = 1000
    STAT_SET['learning_rate'] = [0.01, 0.009]
    STAT_SET['learning_rate_function'] = 'gaussian'
    STAT_SET['reg_rate'] = 0.009
    STAT_SET['momentum_function'] = 'nesterov'
    STAT_SET['momentum'] = 0.8

    NET = Neural_Network()
    NET.initialize_network(STAT_SET)
    TRAIN_RESULTS, TEST_RESULTS, TEST_CONFUSION = NET.Train(0)
    NET.plot_decision_boundary()

    plot_data(range(0, STAT_SET['num_epochs']), [TRAIN_RESULTS[:, 0], TEST_RESULTS[:, 0]], ['Train', 'Test'], 'MPCA')
    plot_data(range(0, STAT_SET['num_epochs']), [TRAIN_RESULTS[:, 1], TEST_RESULTS[:, 1]], ['Train', 'Test'], 'Loss')
    plt.show()

    print "Confusion matrix: \n", TEST_CONFUSION
    print "Max MPCA: ", np.max(TEST_RESULTS[:, 0])
    print "Least Loss: ", np.min(TEST_RESULTS[:, 1])
\end{minted}

\subsection*{Problem 3 Code}

\begin{minted}{python}
"""This function solves Problem 3 of HW1"""

import os
import cPickle as pickle
import numpy as np
import matplotlib.pyplot as plt
from My_Neural_Network import Neural_Network

def generate_dataset(dir_location):
    """generates the CIFAR dataset"""
    arr_txt = [x for x in os.listdir(dir_location) if not x.endswith(".html")]
    print arr_txt
    dataset = []
    for i in range(0, len(arr_txt)):
        file_str = dir_location + '/' + arr_txt[i]
        if 'test' in arr_txt[i]:
            test_data = unpickle(file_str)
        elif 'meta' in arr_txt[i]:
            meta_data = unpickle(file_str)
        else:
            dataset.append(unpickle(file_str))
    im_train_data = []
    im_test_data = test_data['data']
    label_train_data = []
    label_test_data = test_data['labels']
    for i in range(0, len(dataset)):
        data_dict = dataset[i]
        im_train_data.append(data_dict['data'])
        label_train_data.append(data_dict['labels'])
    return np.concatenate(im_train_data), np.concatenate(label_train_data),\
                 im_test_data, np.asarray(label_test_data, dtype=int), meta_data

def unpickle(file_str):
    """unpickles a batch"""
    with open(file_str, 'rb') as fo:
        data_dict = pickle.load(fo)
    return data_dict

def show_images(traindata, trainlabels, num_images):
    """This function plots the first N images of each category"""
    num_classes = np.unique(trainlabels)
    im2plot = []
    for i in range(0, np.size(num_classes)):
        loc = np.where(trainlabels == i)
        im2plot.append(traindata[loc[0][0:num_images], :])

    _, axarr = plt.subplots(3, 10, sharex=True, sharey=True)
    for i in range(0, len(im2plot)):
        for j in range(0, num_images):
            temp = np.zeros((32, 32, 3), dtype=np.uint8)
            im_data = (im2plot[i])[j, :]
            temp[:, :, 0] = np.reshape(im_data[0:32*32*1], (32, 32))
            temp[:, :, 1] = np.reshape(im_data[32*32*1:32*32*2], (32, 32))
            temp[:, :, 2] = np.reshape(im_data[32*32*2:32*32*3], (32, 32))
            axarr[j, i].imshow(temp)

def plot_data(line_x, line_y_list, label_list, title_str):
    """Plotting functions for training and testing loss and mpca"""
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.title(title_str)
    for i in range(0, len(line_y_list)):
        ax.plot(line_x, line_y_list[i], label=label_list[i])
    colormap = plt.cm.brg #nipy_spectral, Set1,Paired
    colors = [colormap(i) for i in np.linspace(0, 1, len(ax.lines))]
    for i, j in enumerate(ax.lines):
        j.set_color(colors[i])
    ax.legend(loc=0)
    plt.show(block=False)

if __name__ == "__main__":
    TRAINDATA, TRAINLABELS, TESTDATA, TESTLABELS, META_DATA = \
    generate_dataset('/home/rakshit/Documents/DL course/Homework 1/Data/cifar-10-batches-py')
    show_images(TRAINDATA, TRAINLABELS, 3)
    # plt.show()

    STAT_SET = {} # Define an empty dictionary
    STAT_SET['network_inputs'] = TRAINDATA.astype(np.float)/255
    STAT_SET['network_targets'] = np.reshape(TRAINLABELS + 1, (TRAINLABELS.size, 1)).astype(np.float)
    STAT_SET['test_inputs'] = TESTDATA.astype(np.float)/255
    STAT_SET['test_targets'] = np.reshape(TESTLABELS + 1, (TESTLABELS.size, 1)).astype(np.float)
    STAT_SET['num_hidden_layers'] = 0
    STAT_SET['num_hidden_nodes'] = []
    STAT_SET['list_activation_functions'] = ['sigmoid', 'sigmoid', 'sigmoid', 'sigmoid']
    STAT_SET['batch_size'] = 1500
    STAT_SET['num_epochs'] = 2
    STAT_SET['learning_rate'] = [0.09, 0.009]
    STAT_SET['learning_rate_function'] = 'gaussian'
    STAT_SET['reg_rate'] = 0.001
    STAT_SET['momentum_function'] = 'nesterov'
    STAT_SET['momentum'] = 0.8

    NET = Neural_Network()
    NET.initialize_network(STAT_SET)
    TRAIN_RESULTS, TEST_RESULTS, TEST_CONFUSION = NET.Train(0)
    plot_data(range(0, STAT_SET['num_epochs']), [TRAIN_RESULTS[:, 0], TEST_RESULTS[:, 0]], ['Train', 'Test'], 'MPCA')
    plot_data(range(0, STAT_SET['num_epochs']), [TRAIN_RESULTS[:, 1], TEST_RESULTS[:, 1]], ['Train', 'Test'], 'Loss')
    plt.show()

    print "Label and Class name: \n", META_DATA
    print "Confusion Matrix: ", TEST_CONFUSION
\end{minted}

To view the source code for the My Neural Network toolbox, please clone/visit the \href{https://RSKothari@bitbucket.org/RSKothari/deep-learning-course.git}{repository}

\end{document}  
