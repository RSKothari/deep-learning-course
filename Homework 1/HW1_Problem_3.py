"""This function solves Problem 3 of HW1"""

import os, sys
import cPickle as pickle
import numpy as np
import matplotlib.pyplot as plt

PATH_TO_HOMEWORK_REPO = '/home/rakshit/Documents/DL course'
sys.path.insert(0, PATH_TO_HOMEWORK_REPO)

from My_NN_package import Neural_Network

def generate_dataset(dir_location):
    """generates the CIFAR dataset"""
    arr_txt = [x for x in os.listdir(dir_location) if not x.endswith(".html")]
    print arr_txt
    dataset = []
    for i in range(0, len(arr_txt)):
        file_str = dir_location + '/' + arr_txt[i]
        if 'test' in arr_txt[i]:
            test_data = unpickle(file_str)
        elif 'meta' in arr_txt[i]:
            meta_data = unpickle(file_str)
        else:
            dataset.append(unpickle(file_str))
    im_train_data = []
    im_test_data = test_data['data']
    label_train_data = []
    label_test_data = test_data['labels']
    for i in range(0, len(dataset)):
        data_dict = dataset[i]
        im_train_data.append(data_dict['data'])
        label_train_data.append(data_dict['labels'])
    return np.concatenate(im_train_data).astype(np.float)/255, np.concatenate(label_train_data),\
                 im_test_data.astype(np.float)/255, np.asarray(label_test_data, dtype=int), meta_data

def unpickle(file_str):
    """unpickles a batch"""
    with open(file_str, 'rb') as fo:
        data_dict = pickle.load(fo)
    return data_dict

def show_images(traindata, trainlabels, num_images):
    """This function plots the first N images of each category"""
    num_classes = np.unique(trainlabels)
    im2plot = []
    for i in range(0, np.size(num_classes)):
        loc = np.where(trainlabels == i)
        im2plot.append(traindata[loc[0][0:num_images], :])

    _, axarr = plt.subplots(3, 10, sharex=True, sharey=True)
    for i in range(0, len(im2plot)):
        for j in range(0, num_images):
            temp = np.zeros((32, 32, 3), dtype=np.uint8)
            im_data = (im2plot[i])[j, :]
            temp[:, :, 0] = np.reshape(im_data[0:32*32*1], (32, 32))
            temp[:, :, 1] = np.reshape(im_data[32*32*1:32*32*2], (32, 32))
            temp[:, :, 2] = np.reshape(im_data[32*32*2:32*32*3], (32, 32))
            axarr[j, i].imshow(temp)

def plot_data(line_x, line_y_list, label_list, title_str):
    """Plotting functions for training and testing loss and mpca"""
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.title(title_str)
    for i in range(0, len(line_y_list)):
        ax.plot(line_x, line_y_list[i], label=label_list[i])
    colormap = plt.cm.brg #nipy_spectral, Set1,Paired
    colors = [colormap(i) for i in np.linspace(0, 1, len(ax.lines))]
    for i, j in enumerate(ax.lines):
        j.set_color(colors[i])
    ax.legend(loc=0)
    plt.show(block=False)

if __name__ == "__main__":
    TRAINDATA, TRAINLABELS, TESTDATA, TESTLABELS, META_DATA = \
    generate_dataset(PATH_TO_HOMEWORK_REPO + '/Data/cifar-10-batches-py')
    show_images(TRAINDATA, TRAINLABELS, 3)
    # plt.show()

    MEAN_TRAIN = np.mean(TRAINDATA, 0)

    STAT_SET = {} # Define an empty dictionary
    STAT_SET['network_inputs'] = TRAINDATA - MEAN_TRAIN
    STAT_SET['network_targets'] = np.reshape(TRAINLABELS + 1, (TRAINLABELS.size, 1)).astype(np.float)
    STAT_SET['test_inputs'] = TESTDATA - MEAN_TRAIN
    STAT_SET['test_targets'] = np.reshape(TESTLABELS + 1, (TESTLABELS.size, 1)).astype(np.float)
    STAT_SET['num_hidden_layers'] = 1
    STAT_SET['num_hidden_nodes'] = [10,]
    STAT_SET['list_activation_functions'] = ['sigmoid', 'sigmoid', 'sigmoid', 'sigmoid']
    STAT_SET['batch_size'] = 500
    STAT_SET['num_epochs'] = 100
    STAT_SET['learning_rate'] = [0.0001, 0.0005]
    STAT_SET['learning_rate_function'] = 'gaussian'
    STAT_SET['reg_rate'] = 0.00001
    STAT_SET['momentum_function'] = 'nesterov'
    STAT_SET['momentum'] = 0.85

    NET = Neural_Network()
    NET.initialize_network(STAT_SET)
    TRAIN_RESULTS, TEST_RESULTS, TEST_CONFUSION = NET.Train(0)
    plot_data(range(0, STAT_SET['num_epochs']), [TRAIN_RESULTS[:, 0], TEST_RESULTS[:, 0]], ['Train', 'Test'], 'MPCA')
    plot_data(range(0, STAT_SET['num_epochs']), [TRAIN_RESULTS[:, 1], TEST_RESULTS[:, 1]], ['Train', 'Test'], 'Loss')
    plt.show()

    print "Label and Class name: \n", META_DATA
    print "Confusion Matrix: \n", TEST_CONFUSION
    print "Max MPCA: ", np.max(TEST_RESULTS[:, 0])
    print "Least Loss: ", np.min(TEST_RESULTS[:, 1])
