from __future__ import division, print_function

import pickle
import sys

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn.functional as F
import torch.nn.utils as utils
from torch import optim
from torch.autograd import Variable
from torchvision import models

import cv2

PATH_TO_HOMEWORK_REPO = '/home/rakshit/Documents/DL course'
fid = open(PATH_TO_HOMEWORK_REPO + '/Data/imagenet1000_clsid_to_human.pkl', 'rb')
IMAGENET_CLASSES = pickle.load(fid)

sys.path.insert(0, PATH_TO_HOMEWORK_REPO)

MODEL_VGG16 = models.vgg16(pretrained=True)
MODEL_VGG16.double()
MODEL_VGG16.eval()
MODEL_VGG16.cuda()

IM_NAME = 'cat'
IMG = cv2.imread(IM_NAME + '.jpg')
#IMG = 128*np.ones((224, 244, 3), dtype=np.uint8)
IMG = IMG[:, :, ::-1] #Convert BGR to RGB

# Pre-process function
def pre_process(img):
    ''' Pre-processing as per Image Net specs'''
    img = cv2.resize(img, (224, 224), interpolation=cv2.INTER_CUBIC)
    img = img.astype(np.float)/255
    img = (img - np.array([0.485, 0.456, 0.406]))/np.array([0.229, 0.224, 0.225])
    img= np.swapaxes(img, 0, 2)
    img = np.swapaxes(img, 1, 2)
    return img

def post_process(img):
    ''' Post-process to reverse pre-process'''
    img = np.swapaxes(img, 0, 2)
    img = np.swapaxes(img, 0, 1)
    img = img*np.array([0.229, 0.224, 0.225])
    img = img + np.array([0.485, 0.456, 0.406])
    img = img - np.min(img)
    img = img/np.max(img)
    img = (img*255).astype(np.uint8)
    return img

IP = torch.from_numpy(pre_process(IMG)).unsqueeze(0)

IP = Variable(IP.cuda(), requires_grad=False)
OUT = F.softmax(MODEL_VGG16.forward(IP))
OUT = np.squeeze(OUT.cpu().data.numpy())

LOC = np.argmax(OUT)
print("Prediction probability: ", OUT[LOC])
print(IMAGENET_CLASSES[LOC])

TARGET_CLASS = 497
NUM_EPOCHS = 1000
LR = 2e-1
LAMBDA = 5e-1
OP_IMG = Variable(IP.data.clone().cuda(), requires_grad=True)
OPTIMIZER = optim.SGD([OP_IMG], lr=LR, momentum=0.9)

def clip_images(A, B, eps):
    temp = A - B
    #print('Min: {}, Max: {}'.format(
    #    np.min(temp.numpy()), np.max(temp.numpy())))
    loc = temp < -eps
    B[loc] = A[loc] - eps
    loc = temp > eps
    B[loc] = A[loc] + eps
    return B

n_iter = 0
cond = 1
while cond:
    OPTIMIZER.zero_grad()
    class_pred = MODEL_VGG16.forward(OP_IMG)
    class_loss = -class_pred[0, TARGET_CLASS]
    class_prob = F.softmax(class_pred)[0, TARGET_CLASS]

    L_loss = LAMBDA*torch.norm(OP_IMG - IP + 1e-8, p=2)/2
    L = class_loss + L_loss
    print('iter: {}, total loss: {}, class loss: {}, L2 Loss: {}, Target class prob: {}'.
          format(n_iter, L.data[0], class_loss.data[0], L_loss.data[0], class_prob.data[0]))

    L.backward()
    OP_IMG.data = clip_images(IP.data, OP_IMG.data, 0.01)
    OPTIMIZER.step()
    n_iter += 1
    if n_iter > NUM_EPOCHS or class_prob.data[0] >= 0.999:
        cond = 0

OP = post_process(OP_IMG.cpu().data.numpy()[0])
DIFF = (OP_IMG.cpu().data - IP.cpu().data).numpy()[0]
DIFF = post_process(DIFF)

f, plts = plt.subplots(1, 3)
plts[0].imshow(IMG)
plts[0].set_title('IP. Class: ' + IMAGENET_CLASSES[LOC] + ' P: ' + str(OUT[LOC]))
plts[1].imshow(OP)
plts[1].set_title('OP. Class: ' + IMAGENET_CLASSES[TARGET_CLASS] + ' P: ' + str(class_prob.data[0]))
plts[2].imshow(DIFF)
plts[2].set_title('Difference Image.')
plt.show()

cv2.imwrite(IM_NAME + '_modified.jpg', OP[:,:,::-1])
cv2.imwrite(IM_NAME + '_difference.jpg', DIFF[:,:,::-1])
