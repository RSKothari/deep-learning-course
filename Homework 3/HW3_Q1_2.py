from __future__ import division, print_function

import pickle
import sys

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn.functional as F
import torch.nn.utils as utils
from torch import optim
from torch.autograd import Variable
from torchvision import models

import cv2

PATH_TO_HOMEWORK_REPO = '/home/rakshit/Documents/DL course'
fid = open(PATH_TO_HOMEWORK_REPO + '/Data/imagenet1000_clsid_to_human.pkl', 'rb')
IMAGENET_CLASSES = pickle.load(fid)

sys.path.insert(0, PATH_TO_HOMEWORK_REPO)

MODEL_VGG16 = models.vgg16(pretrained=True)
MODEL_VGG16.double()
MODEL_VGG16.eval()
#MODEL_VGG16.cuda()

IM_NAME = 'cat_modified'
IMG = cv2.imread(IM_NAME + '.jpg')
IMG = cv2.cvtColor(IMG, cv2.COLOR_BGR2GRAY)
IMG_t = np.zeros((224, 224, 3), dtype=np.uint8)
IMG_t[:,:,0] = IMG
IMG_t[:,:,1] = IMG
IMG_t[:,:,2] = IMG
IMG = IMG_t
'''
rows, cols, _ = IMG.shape
M = cv2.getRotationMatrix2D((cols/2,rows/2),30,1)
IMG = cv2.warpAffine(IMG, M, (cols, rows))
'''
#IMG = np.delete(IMG, list(range(120,220)), axis=1)
#IMG = 128*np.ones((224, 244, 3), dtype=np.uint8)
#IMG = IMG[:, :, ::-1] #Convert BGR to RGB

# Pre-process function
def pre_process(img):
    ''' Pre-processing as per Image Net specs'''
    img = cv2.resize(img, (224, 224), interpolation=cv2.INTER_CUBIC)
    img = img.astype(np.float)/255
    img = (img - np.array([0.485, 0.456, 0.406]))/np.array([0.229, 0.224, 0.225])
    img= np.swapaxes(img, 0, 2)
    img = np.swapaxes(img, 1, 2)
    return img

def post_process(img):
    ''' Post-process to reverse pre-process'''
    img = np.swapaxes(img, 0, 2)
    img = np.swapaxes(img, 0, 1)
    img = img*np.array([0.229, 0.224, 0.225])
    img = img + np.array([0.485, 0.456, 0.406])
    img = img - np.min(img)
    img = img/np.max(img)
    img = (img*255).astype(np.uint8)
    return img

IP = torch.from_numpy(pre_process(IMG)).unsqueeze(0)

IP = Variable(IP, requires_grad=False)
OUT = F.softmax(MODEL_VGG16.forward(IP))
OUT = np.squeeze(OUT.data.numpy())

LOC = np.argmax(OUT)
print("Prediction probability: ", OUT[LOC])
print(IMAGENET_CLASSES[LOC])
'''
OP_IMG = Variable(IP.data.clone().cuda(), requires_grad=False)

class_pred = MODEL_VGG16.forward(OP_IMG)
class_loss = -class_pred[0, TARGET_CLASS]
class_prob = F.softmax(class_pred)[0, TARGET_CLASS]
'''

f, plts = plt.subplots()
plts.imshow(IMG)
plts.set_title('IP. Class: ' + IMAGENET_CLASSES[LOC] + ' P: ' + str(OUT[LOC]))
plt.show()