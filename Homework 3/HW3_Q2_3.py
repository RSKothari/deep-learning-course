import pickle

import numpy as np
import torch
import torch.nn.functional as F
import torch.nn.utils as utils
from matplotlib import pyplot as plt
from torch import optim
from torch.autograd import Variable

import cv2

M_LEFT = np.loadtxt('prob-a1.txt')
M_TOP = np.loadtxt('prob-a2.txt')
M_RIGHT = np.loadtxt('prob-a3.txt')
M_BOT = np.loadtxt('prob-a4.txt')
WORLD_INFO = np.loadtxt('world.txt')

REWARDS = np.loadtxt('rewards.txt').squeeze()

WORLD = cv2.imread('maze.jpg')
WORLD = cv2.resize(WORLD, (1350, 1350), interpolation=cv2.INTER_CUBIC)
AGENT = cv2.imread('gnome.png')

def render_world_view(world, agent, state, grid_size):
    hw, ww, _ = world.shape
    hw, ww = int(hw/9), int(ww/9)
    agent = cv2.resize(agent, (ww, hw), interpolation=cv2.INTER_CUBIC)
    col = np.floor(state/grid_size)
    row = state%grid_size
    x1, x2 = int(row*hw), int((row+1)*hw)
    y1, y2 = int(col*ww), int((col+1)*ww)
    world[x1:x2, y1:y2, :] = agent
    return world

def render_world_states(world, world_mat, q_table, grid_size):
    num_states = 81
    hw, ww, _ = world.shape
    hw, ww = int(hw/9), int(ww/9)

    for state in range(0, num_states):
        dist = np.dot(q_table, encodeState(state, world_mat, 78, grid_size).numpy())
        dist = dist - np.min(dist)
        if np.sum(dist) != 0.0:
            dist = dist/np.max(dist)
        col = np.floor(state/grid_size)
        row = state%grid_size
        y, x = int(row*hw + 0.5*hw), int(col*ww + 0.5*ww)
        scale = dist[0]
        if scale != 0: ax.quiver(x, y,
                                 scale*hw*-1/3, 0, angles='xy',
                                 scale_units='xy', scale=1.,
                                 width=0.004)
        scale = dist[1]
        if scale != 0: ax.quiver(x, y,
                                 0, scale*-ww*1/3, angles='xy',
                                 scale_units='xy', scale=1.,
                                 width=0.004)
        scale = dist[2]
        if scale != 0: ax.quiver(x, y,
                                 scale*hw*1/3, 0, angles='xy',
                                 scale_units='xy', scale=1.,
                                 width=0.004)
        scale = dist[3]
        if scale != 0: ax.quiver(x, y,
                                 0, scale*ww*1/3, angles='xy',
                                 scale_units='xy', scale=1.,
                                 width=0.004)

def generate_tmatrix(data, N):
    t_matrix = np.zeros((N, N), dtype=float)
    for i in range(0, len(data)):
        current_state, next_state, p = data[i, :]
        t_matrix[int(current_state)-1, int(next_state)-1] = p
    return t_matrix

def takeAction(MDP, current_state, current_action):
    next_state = np.argmax(MDP[2][current_state, :, current_action])
    '''
    dist = MDP[2][current_state, :, current_action]
    dist = dist - np.min(dist)
    dist = dist/np.sum(dist)
    next_state = np.random.choice(list(range(0,np.size(dist))), p=dist)
    '''
    reward = MDP[3][next_state]
    cont_bool = 1
    if reward < 0 or reward > 0:
        #print('Reward: {}'.format(reward))
        cont_bool = 0
    return (cont_bool, next_state, reward)

'''Code sample courtesy rayreng - StackOverFlow'''
def sub2ind(array_shape, rows, cols):
    ind = cols*array_shape[1] + rows
    #ind[ind < 0] = -1
    #ind[ind >= array_shape[0]*array_shape[1]] = -1
    return ind

def ind2sub(array_shape, ind):
    #ind[ind < 0] = -1
    #ind[ind >= array_shape[0]*array_shape[1]] = -1
    cols = int(ind / array_shape[1])
    rows = ind % array_shape[1]
    return (rows, cols)

def encodeState(current_state, world_mat, goal_loc, grid_size):
    out = -1*np.ones((20, 1), dtype=np.float)
    row, col = ind2sub([grid_size, grid_size], current_state)
    win_row = [row - 1, row - 1, row - 1, row, row, row, row + 1, row + 1, row + 1]
    win_col = [col - 1, col, col + 1, col - 1, col, col + 1, col - 1, col, col + 1]
    win_states = sub2ind([grid_size, grid_size], np.array(win_row), np.array(win_col))
    
    for i in range(0, len(win_states)):
        if win_states[i] >= 0 and win_states[i] < 81:
            out[i, 0] = world_mat[win_states[i]]
    out[i+1 : i + 10, 0] = np.array(win_states)
    goal_row, goal_col = ind2sub([grid_size, grid_size], goal_loc)
    dist = np.linalg.norm(np.array([row, col]) - np.array([goal_row, goal_col]), ord=2)
    out[-1, 0] = dist
    out[-1, 0] = 1
    out = torch.from_numpy(out)
    out = out.float()
    return out

def pickAction(currentFeatVector, Q_network, eps):
    switch = np.random.rand(1)

    if switch > eps:
        T = np.dot(Q_network.data.numpy(), currentFeatVector.data.numpy())
        return np.argmax(T.squeeze())
    else:
        return np.random.choice(list(range(4)))

f = plt.figure()
ax = f.add_subplot(111)
I_RENDERED = ax.imshow(WORLD)
ax.set_title('Current View')
f.show()

STATES = list(range(0, 81))
ACTIONS = [0, 1, 2, 3]
T_MATRIX = np.zeros((81, 81, 4), dtype=float)
T_MATRIX[:, :, 0] = generate_tmatrix(M_LEFT, 81)
T_MATRIX[:, :, 1] = generate_tmatrix(M_TOP, 81)
T_MATRIX[:, :, 2] = generate_tmatrix(M_RIGHT, 81)
T_MATRIX[:, :, 3] = generate_tmatrix(M_BOT, 81)

MDP = (STATES, ACTIONS, T_MATRIX, REWARDS)

W_DIFF = []
L = []

DISCOUNT_FACTOR = 0.99
LR = 1e-6
EPS = 0.9
Q_NET = Variable(torch.rand(len(ACTIONS), 20), requires_grad=True)
OPTIMIZER = optim.SGD([Q_NET], lr=LR, momentum=0.5)

for epoch in range(0, 5000):
    playing = True
    current_state = 2
    currentFeatVector = Variable(encodeState(current_state, WORLD_INFO, 78, 9), requires_grad=False)

    if epoch%2000 == 0:
        EPS = 0.5*EPS

    weights_norm = []
    L_epoch = []
    while playing:
        temp = Q_NET.clone().data.numpy()

        OPTIMIZER.zero_grad()
        action = pickAction(currentFeatVector, Q_NET, EPS)
        playing, new_state, reward = takeAction(MDP, current_state, action)
        newFeatVector = Variable(encodeState(new_state, WORLD_INFO, 78, 9), requires_grad=False)

        V_new = torch.mm(Q_NET, newFeatVector)
        V_old = torch.mm(Q_NET, currentFeatVector)
        Q_mod = Variable(torch.from_numpy(np.array([reward], dtype=np.float32))) + DISCOUNT_FACTOR*torch.max(V_new)
        loss = 0.5*torch.pow(V_old[action] - Q_mod, 2)

        loss.backward()
        OPTIMIZER.step()
        current_state = new_state
        currentFeatVector = newFeatVector

        temp = np.linalg.norm(temp - Q_NET.data.numpy(), ord=2)
        weights_norm.append(temp)
        L_epoch.append(loss.data.numpy())
        state3 = np.dot(Q_NET.data.numpy(), encodeState(2, WORLD_INFO, 78, 9).numpy())
        #print('Episode: {}. Current State: {}. Action: {}. Reward: {}. State 3: {}'.format(epoch, current_state, action, reward, state3.T))
    W_DIFF.append(np.mean(weights_norm))
    L.append(np.mean(L_epoch))
    print('Episode: {}. Current State: {}. Reward: {}. Current loss: {}. State 3: {}'.format(epoch, current_state, reward, L[-1], state3.T))

render_world_states(WORLD, WORLD_INFO, Q_NET.data.numpy(), 9)
plt.show()

f, ax = plt.subplots(1, 2)
ax[0].plot(W_DIFF)
ax[0].set_title('Norm of difference')
ax[1].plot(L)
ax[1].set_title('Loss')
plt.show()
