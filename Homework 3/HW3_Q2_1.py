import pickle

import numpy as np
from matplotlib import pyplot as plt
import copy
import cv2

# Flag to display the rendered image
DISPLAY_SIM = False

M_LEFT = np.loadtxt('prob-a1.txt')
M_TOP = np.loadtxt('prob-a2.txt')
M_RIGHT = np.loadtxt('prob-a3.txt')
M_BOT = np.loadtxt('prob-a4.txt')

REWARDS = np.loadtxt('rewards.txt').squeeze()

WORLD = cv2.imread('maze.jpg')
WORLD = cv2.resize(WORLD, (1350, 1350), interpolation=cv2.INTER_CUBIC)
AGENT = cv2.imread('gnome.png')

def render_world_view(world, agent, state, gridsize):
    hw, ww, _ = world.shape
    hw, ww = int(hw/9), int(ww/9)
    agent = cv2.resize(agent, (ww, hw), interpolation=cv2.INTER_CUBIC)
    col = np.floor(state/gridsize)
    row = state%gridsize
    x1, x2 = int(row*hw), int((row+1)*hw)
    y1, y2 = int(col*ww), int((col+1)*ww)
    world[x1:x2, y1:y2, :] = agent
    return world

def render_world_states(world, q_table, gridsize):
    num_states, num_actions = q_table.shape
    hw, ww, _ = world.shape
    hw, ww = int(hw/9), int(ww/9)

    for state in range(0, num_states):
        dist = q_table[state, :]
        dist = dist - np.min(dist)
        if np.sum(dist) != 0.0:
            dist = dist/np.max(dist)
        col = np.floor(state/gridsize)
        row = state%gridsize
        y, x = int(row*hw + 0.5*hw), int(col*ww + 0.5*ww)
        scale = dist[0]
        if scale != 0: ax.quiver(x, y,
                                 scale*hw*-1/3, 0, angles='xy',
                                 scale_units='xy', scale=1.,
                                 width=0.004)
        scale = dist[1]
        if scale != 0: ax.quiver(x, y,
                                 0, scale*-ww*1/3, angles='xy',
                                 scale_units='xy', scale=1.,
                                 width=0.004)
        scale = dist[2]
        if scale != 0: ax.quiver(x, y,
                                 scale*hw*1/3, 0, angles='xy',
                                 scale_units='xy', scale=1.,
                                 width=0.004)
        scale = dist[3]
        if scale != 0: ax.quiver(x, y,
                                 0, scale*ww*1/3, angles='xy',
                                 scale_units='xy', scale=1.,
                                 width=0.004)

def generate_tmatrix(data, N):
    t_matrix = np.zeros((N, N), dtype=float)
    for i in range(0, len(data)):
        current_state, next_state, p = data[i, :]
        t_matrix[int(current_state)-1, int(next_state)-1] = p
    return t_matrix

def takeAction(MDP, current_state, current_action):
    #num_states = len(MDP[0])
    #dist = MDP[2][current_state, :, current_action].squeeze()
    #dist = dist - np.min(dist)
    #dist = dist/np.sum(dist)
    #next_state = np.random.choice(list(range(num_states)), p=dist)
    next_state = np.argmax(MDP[2][current_state, :, current_action])
    reward = MDP[3][next_state]
    cont_bool = 1
    if reward == -1.0 or reward == +1.0:
        #print('Reward: {}'.format(reward))
        cont_bool = 0
    return (cont_bool, next_state, reward)

def pickAction(q_table, current_state, eps):
    _, num_actions = np.shape(q_table)
    dist = q_table[current_state, :]
    dist = dist - np.min(dist)

    switch = np.random.rand(1)
    if np.sum(dist) == 0.0:
        switch = 0.0

    if switch > eps:
        #dist = dist/np.sum(dist)
        #return np.random.choice(list(range(num_actions)), p=dist)
        return np.argmax(dist)
    else:
        temp = np.random.choice(list(range(num_actions)))
        return temp
    '''
    if np.sum(dist) == 0.0:
        dist = np.ones(num_actions, dtype=np.float)
    dist = dist/np.sum(dist)
    return np.random.choice(list(range(num_actions)), p=dist)
    '''

def updateQtable(q_table, current_state, action, new_state, reward, alpha, gamma, eps):
    _, num_actions = np.shape(q_table)
    '''
    switch = np.random.rand(1)
    if switch > eps:
        #Select best action based on greedy
        V = np.max(q_table[new_state, :])
    elif switch <= eps:
        #Select a random action
        future_action = np.random.choice(list(range(num_actions)))
        V = q_table[new_state, future_action]
    '''
    V = np.max(q_table[new_state, :])
    q_table[current_state, action] = (1 - alpha)*q_table[current_state, action] + alpha*(reward + gamma*V)
    #print(q_table[current_state, action])
    return q_table

f = plt.figure()
ax = f.add_subplot(111)
I_RENDERED = ax.imshow(WORLD)
ax.set_title('Current view')
#f.show()

STATES = list(range(1, 81 + 1))
ACTIONS = [0, 1, 2, 3]
T_MATRIX = np.zeros((81, 81, 4), dtype=float)
T_MATRIX[:, :, 0] = generate_tmatrix(M_LEFT, 81)
T_MATRIX[:, :, 1] = generate_tmatrix(M_TOP, 81)
T_MATRIX[:, :, 2] = generate_tmatrix(M_RIGHT, 81)
T_MATRIX[:, :, 3] = generate_tmatrix(M_BOT, 81)

DISCOUNT_FACTOR = 0.99
LR = 0.01
EPS = 0.2

MDP = (STATES, ACTIONS, T_MATRIX, REWARDS)
Q_TABLE = np.zeros((len(STATES), len(ACTIONS)), dtype=np.float)
Q_TABLE_DIFF = []

SUCCESS_COUNT = 0
#STOP_TRAIN = False
episode = 0
while episode < 10000:
    playing = True
    current_state = 2

    if episode%5000 == 0:
        EPS = EPS*0.5

    val = []
    while playing:
        if DISPLAY_SIM:
            VIEW = render_world_view(WORLD, AGENT, current_state, 9)
            I_RENDERED.set_data(VIEW)
            f.canvas.draw()
            f.show()
        temp = copy.copy(Q_TABLE)
        action = pickAction(Q_TABLE, current_state, EPS)
        playing, new_state, reward = takeAction(MDP, current_state, action)
        Q_TABLE = updateQtable(Q_TABLE, current_state,
                               action, new_state, reward,
                               LR, DISCOUNT_FACTOR, EPS)
        current_state = new_state
        val.append(np.linalg.norm(Q_TABLE - temp, ord=2))
        if not playing and reward == -1.0:
            SUCCESS_COUNT = 0
        elif not playing and reward == 1.0:
            SUCCESS_COUNT += 1
    Q_TABLE_DIFF.append(np.mean(val))
    print('Ended episode {}. Final reward for state 3: {}'.format(episode, Q_TABLE[2, :]))
    episode += 1

    if SUCCESS_COUNT == 50:
        #STOP_TRAIN = True
        print('Converged in {} Epochs. Eps: {}'.format(episode, EPS))

pickle.dump(Q_TABLE, open("Q table.p", "wb"))
render_world_states(WORLD, Q_TABLE, 9)
plt.show()

f, ax = plt.subplots()
ax.plot(Q_TABLE_DIFF)
ax.set_title('Difference in Q table vs Epochs')
plt.show()