"""File contains the Neural Network class to be imported.
Network parameters to be given in as a dictionary"""
import sys
from .ActivationFunctions import ListOfFuncs
import matplotlib.pyplot as plt
import numpy as np

class Neural_Network(object):
    """This class defines all the functions available for training a variable
        size feedforward, stochastic gradient descent Neural Network"""

    def __init__(self):
        """List out all the default paramteres"""
        self.num_hidden_layers = 1
        self.num_hidden_nodes = 10
        self.activation_functions = ListOfFuncs()
        self.network_inputs = []
        self.network_targets = []
        self.network_targets_nn = []
        self.test_inputs = []
        self.test_targets = []
        self.num_obs = []
        self.num_ip_dims = []
        self.num_op_dims = []
        self.op_classtypes = []
        self.list_activation_functions = []
        self.weights = []
        self.momemtum_val = []
        self.bias = []
        self.class_proportions = []
        self.batch_size = []
        self.num_epochs = []
        self.layer_op = []
        self.reg_rate = 0
        self.learning_rate = []
        self.learning_rate_function = []
        self.momemtum = 0
        self.momemtum_function = []
        self.dropout = False
        self.dropout_rate = 0
        self.dropout_layer_idx = []
        self.dropout_cond = []
        self.Z = []

    def softmax_func(self, x, der):
        """This function computes the Softmax action on an input 1D numpy array.
        The input can be in any arbitary real range"""
        N = (x.T - np.max(x, 1)).T # Subtract the max to make it numerically stable
        N = np.exp(N) # Find the exp for every element
        D = np.sum(N, 1) # Sum the exponent along columns
        D = np.tile(D, (x.shape[1], 1)).T # Repmat and Transpose
        fs_x = N/D # Elementwise division
        fs_x = fs_x + 1e-5
        if der == 0:
            return fs_x

    def GenerateDropoutMatrix(self, batch_size, dim_size):
        """ This function generates a dropout condition matrix to be pre-multiplied
        with the input of at a layer"""
        out = np.random.rand(batch_size, dim_size)
        out = out > self.dropout_rate
        return out

    def Update_Dropout(self, BatchIdx):
        """The purpose of this function is to update the nodes which will be dropped out"""
        self.dropout_cond = list()

        if self.num_hidden_layers != 0:
            for i in range(0, self.num_hidden_layers + 1):
                if i == 0:
                    if i in self.dropout_layer_idx and self.dropout:
                        D = self.GenerateDropoutMatrix(BatchIdx.size, self.num_ip_dims)
                    else:
                        D = np.ones((BatchIdx.size, self.num_ip_dims), dtype=np.float)
                elif i != 0:
                    if i in self.dropout_layer_idx and self.dropout:
                        D = self.GenerateDropoutMatrix(BatchIdx.size, self.num_hidden_nodes[i-1])
                    else:
                        D = np.ones((BatchIdx.size, self.num_hidden_nodes[i-1]), dtype=np.float)
                self.dropout_cond.append(D)
        else:
            if 0 in self.dropout_layer_idx and self.dropout:
                D = self.GenerateDropoutMatrix(BatchIdx.size, self.num_ip_dims)
            else:
                D = np.ones((BatchIdx.size, self.num_ip_dims), dtype=np.float)
                self.dropout_cond.append(D)

    def initialize_weights(self):
        """This routine initializes the weights in a NN with 0 values"""
        self.weights = list()
        self.bias = list()

        if self.num_hidden_layers != 0:
            for i in range(0, self.num_hidden_layers + 1):
                # You must ensure that you set the Weights joining the input and output layers
                if i == 0:
                    # Weights and bias between input and hidden layer
                    W = 2*np.random.rand(self.num_ip_dims, self.num_hidden_nodes[i]) - 1
                    B = 2*np.random.rand(1, self.num_hidden_nodes[i]) - 1
                    self.weights.append(W)
                    self.bias.append(B)

                elif i == self.num_hidden_layers:
                    # Weights and bias between last hidden layer and softmax
                    W = 2*np.random.rand(self.num_hidden_nodes[i-1], self.num_op_dims) - 1
                    B = 2*np.random.rand(1, self.num_op_dims) - 1
                    self.weights.append(W)
                    self.bias.append(B)

                else:
                    # Weights and bias between hidden layers
                    W = 2*np.random.rand(self.num_hidden_nodes[i-1], self.num_hidden_nodes[i]) - 1
                    B = 2*np.random.rand(1, self.num_hidden_nodes[i]) - 1
                    self.weights.append(W)
                    self.bias.append(B)
        else:
            W = 2*np.random.rand(self.num_ip_dims, self.num_op_dims) - 1
            B = 2*np.random.rand(1, self.num_op_dims) - 1
            self.weights.append(W)
            self.bias.append(B)

        self.momemtum_val = [0]*len(self.weights)

    def initialize_network(self, params):
        """This function is used to initialize the network and overwrite default variables"""
        self.network_inputs = params['network_inputs']
        self.network_targets = params['network_targets']
        self.test_inputs = params['test_inputs']
        self.test_targets = params['test_targets']
        self.num_hidden_layers = params['num_hidden_layers']
        self.num_hidden_nodes = params['num_hidden_nodes']
        self.num_obs = self.network_inputs.shape[0]
        self.num_ip_dims = self.network_inputs.shape[1]
        self.num_op_dims = int(np.max(np.unique(self.network_targets)))
        self.op_classtypes = np.unique(self.network_targets)
        self.list_activation_functions = params['list_activation_functions']
        self.batch_size = params['batch_size']
        self.num_epochs = params['num_epochs']
        self.reg_rate = params['reg_rate']
        self.learning_rate = params['learning_rate']
        self.learning_rate_function = params['learning_rate_function']
        self.momemtum_function = params['momentum_function']
        self.momemtum = params['momentum']
        if 'dropout' in params:
            self.dropout = params['dropout']
            self.dropout_layer_idx = params['dropout_layer_idx']
            self.dropout_rate = params['dropout_rate']

        output_class_idx = []
        for i in range(0, len(self.op_classtypes)):
            output_class_idx.append(self.network_targets == self.op_classtypes[i])
            self.class_proportions.append(len(output_class_idx[i]))

        self.class_proportions = np.array(self.class_proportions, dtype=float)
        self.class_proportions = self.class_proportions/sum(self.class_proportions)

        if self.network_inputs.shape[0] != self.network_targets.shape[0]:
            print("Error. The number of Ip and Op observations should be the same")

        if self.network_inputs.shape[0] < self.network_inputs.shape[1]:
            print("Warning. Input has more dimensions than observations")

        #-1 because ind2vec starts from 0
        self.network_targets_nn = self.ind2vec(self.network_targets - 1, self.num_op_dims)
        self.initialize_weights()

    def ind2vec(self, ind, N=None):
        """This function convert target to 1 hot-vector representation"""
        ind = np.squeeze(ind)
        ind = np.asarray(ind)
        if N is None:
            N = ind.max() + 1
        return (np.arange(N) == ind[:, None]).astype(int)

    def forward(self, batch, cond):
        """This routine is used to peform a forward pass with the given batch"""
        if cond == 1:
            self.layer_op = list()
            self.Z = list()
            # Training data
            if self.num_hidden_layers != 0:
                for i in range(0, self.num_hidden_layers + 1):
                    D = self.dropout_cond[i]
                    D = D.astype(np.float)/(1 - self.dropout_rate)
                    # Iterate through all weights and multiply
                    if i == 0:
                        # Input layer to the first Hidden Layer"""
                        self.Z.append(np.dot(D*batch, self.weights[i]) + np.tile(self.bias[i], (batch.shape[0], 1)))
                        self.layer_op.append(self.ApplyActivationFunc(self.Z[i], i, 0))

                    elif i == self.num_hidden_layers:
                        # At the junction between the final layer and output layer.
                        # There won't be an activation function over here.
                        self.Z.append(np.dot(D*self.layer_op[i - 1], self.weights[i]) + np.tile(self.bias[i], (batch.shape[0], 1)))
                        self.layer_op.append(self.Z[i])

                    else:
                        # Input between hidden layer i - 1 and i"""
                        self.Z.append(np.dot(D*self.layer_op[i - 1], self.weights[i]) + np.tile(self.bias[i], (batch.shape[0], 1)))
                        self.layer_op.append(self.ApplyActivationFunc(self.Z[i], i, 0))

            else:
                D = self.dropout_cond[0]
                D = D.astype(np.float)/(1 - self.dropout_rate)
                self.Z.append(np.dot(D*batch, self.weights[0]) + np.tile(self.bias[0], (batch.shape[0], 1)))
                self.layer_op.append(self.Z[0]) # Note, there is no activation function here
                # Apply softmax layer
            return self.softmax_func(self.layer_op[-1], 0)
        else:
            # Testing data
            test_layer_op = list()
            test_layer_z = list()
            if self.num_hidden_layers != 0:
                for i in range(0, self.num_hidden_layers + 1):
                    # Iterate through all weights and multiply
                    if i == 0:
                        # Input layer to the first Hidden Layer"""
                        test_layer_z.append(np.dot(batch, self.weights[i]) + np.tile(self.bias[i], (batch.shape[0], 1)))
                        test_layer_op.append(self.ApplyActivationFunc(test_layer_z[i], i, 0))

                    elif i == self.num_hidden_layers:
                        # At the junction between the final layer and output layer.
                        # There won't be an activation function over here.
                        test_layer_z.append(np.dot(test_layer_op[i - 1], self.weights[i]) + np.tile(self.bias[i], (batch.shape[0], 1)))
                        test_layer_op.append(test_layer_z[i])

                    else:
                        # Input between hidden layer i - 1 and i"""
                        test_layer_z.append(np.dot(test_layer_op[i - 1], self.weights[i]) + np.tile(self.bias[i], (batch.shape[0], 1)))
                        test_layer_op.append(self.ApplyActivationFunc(test_layer_z[i], i, 0))

            else:
                test_layer_z.append(np.dot(batch, self.weights[0]) + np.tile(self.bias[0], (batch.shape[0], 1)))
                test_layer_op.append(test_layer_z[0]) # Note, there is no activation function here
                # Apply softmax layer
            return self.softmax_func(test_layer_op[-1], 0)

    def ApplyActivationFunc(self, Ip, i, der):
        """Takes the index at which we are at, finds the necessary function and applies it"""
        method = getattr(self.activation_functions, self.list_activation_functions[i])
        return method(Ip, der)

    def GetLearningRate(self, i, LRmin, LRmax):
        """This functions returns a variable learning rate as move on to higher epochs. 
        Higher the epoch number, smaller the learning rate. Follows a Gaussian distribution"""

        if 'gaussian' in self.learning_rate_function: 
            return LRmin + (LRmax - LRmin)*np.exp(-0.5*pow(4*float(i)/self.num_epochs, 2))
        elif 'constant' in self.learning_rate_function:
            return float(LRmax)
        elif 'linear' in self.learning_rate_function:
           return (float(i)*(LRmin - LRmax)/self.num_epochs) + LRmax
        else:
            print("Learning rate function not specified")
            sys.exit()

    def Train(self, plot_on):
        """The function to begin training using Stochastic Gradient Descent"""
        perf_train = []
        perf_test = []
        for i in range(0, self.num_epochs):
            perf_batch_train = []
            perf_batch_test = []
            BatchIdx = self.GenerateBatchIdx()
            learning_rate = self.GetLearningRate(i, self.learning_rate[0], self.learning_rate[1])
            for j in range(0, len(BatchIdx)):
                self.Update_Dropout(BatchIdx[j])
                network_output = self.forward(self.network_inputs[BatchIdx[j], :], 1)
                network_output_test = self.forward(self.test_inputs, 0)
                mpca_train, loss_train, _ = self.ComputeLoss(network_output, self.network_targets[BatchIdx[j], :])
                mpca_test, loss_test, confusionmat_test = self.ComputeLoss(network_output_test, self.test_targets)
                # We only use the training data to update weights
                self.UpdateWeights(self.network_inputs[BatchIdx[j], :], self.network_targets_nn[BatchIdx[j], :], network_output, learning_rate, self.reg_rate)             
                perf_batch_train.append([mpca_train, loss_train])
                perf_batch_test.append([mpca_test, loss_test])

            print("Epoch: ", i, " Learning rate: ", learning_rate, " MPCA&Loss: ", np.mean(np.asarray(perf_batch_train, dtype=float), 0))
            perf_train.append(np.mean(np.asarray(perf_batch_train, dtype=float), 0))
            perf_test.append(np.mean(np.asarray(perf_batch_test, dtype=float), 0))

        if plot_on:
            self.PlotPerformance(np.asarray(perf_train, dtype=float), 'Training Performance')
            self.PlotPerformance(np.array(perf_test, dtype=float), 'Test Performance')

        return np.asarray(perf_train, dtype=float), np.asarray(perf_test, dtype=float), confusionmat_test

    def PlotPerformance(self, Perf, title_str):
        plt.figure()
        plt.plot(range(0, self.num_epochs), Perf[:,0], color='r', label='MPCA')
        plt.plot(range(0, self.num_epochs), Perf[:,1], color='g', label='Loss')
        plt.title(title_str)
        plt.show()

    def GenerateBatchIdx(self):
        """Generate Idxs for each batch. If Stochastic then use BatchSize = 1"""
        BatchIdx = []
        NumOfBatches = int(np.ceil(self.num_obs/self.batch_size))
        T = np.random.permutation(np.arange(self.num_obs))
        for i in range(0, NumOfBatches):
            x = self.batch_size*i
            y = np.min([self.batch_size*(i+1), len(T)])
            BatchIdx.append(T[x:y])
        return BatchIdx

    def ComputeLoss(self, networkOutput, GroundTruth):
        """This function computes the loss and mean per-class accuracy from the target class"""
        T = self.ind2vec(GroundTruth - 1, self.num_op_dims) # Ground Truth starts from 1
        ClassifiedOut = networkOutput.argmax(axis=1) + 1
        #Loss = -1*np.sum(np.log(np.diag(np.dot(T, networkOutput.T))))
        ''' Modification
        This modification is done to ensure low RAM usage while slowing the processing.
        For smaller dataset, it would be beneficial to activate the high RAM condition.
        '''
        Loss = 0
        for i in range(0, GroundTruth.shape[0]):
            Loss = Loss - 1*np.log(np.dot(T[i, :], networkOutput.T[:, i]))
        #Loss = -Loss

        confusionMat = self.GenerateConfusionMatrix(np.squeeze(GroundTruth), ClassifiedOut)
        mpca = np.nanmean(np.diag(confusionMat)/np.sum(confusionMat, 1))
        return mpca, Loss, confusionMat

    def GenerateConfusionMatrix(self, GroundTruth, ClassifiedOut):
        """This function computes the confusion matrix for given input"""
        confusionMat = np.zeros((self.num_op_dims, self.num_op_dims))
        for i in range(0, GroundTruth.shape[0]):
            confusionMat[int(GroundTruth[i]-1), int(ClassifiedOut[i]-1)] += 1
        return confusionMat

    def UpdateWeights(self, batch, batch_targets, networkOutput, alpha, reg):
        """This function is used to update the weights & biases in the Neural Network"""
        p = [0]*(self.num_hidden_layers+1)
        w_gradients = [0]*(self.num_hidden_layers+1)
        dc_dw = [0]*(self.num_hidden_layers+1)
        db_dw = [0]*(self.num_hidden_layers+1)

        for i in range(self.num_hidden_layers+1, 0, -1):
            if i-1 == self.num_hidden_layers:
                # p[i-1] = self.softmax_func(self.layer_op[-1], 1)
                p[i-1] = networkOutput - batch_targets
            else:
                p[i-1] = np.dot(p[i], self.weights[i].T)*self.ApplyActivationFunc(self.Z[i-1], i-1, 1)

            if i-1 == 0:
                D = self.dropout_cond[0]
                D = D.astype(np.float)/(1 - self.dropout_rate)
                w_gradients[i-1] = np.dot((D*batch).T, p[i-1])
            else:
                D = self.dropout_cond[i-1]
                D = D.astype(np.float)/(1 - self.dropout_rate)
                w_gradients[i-1] = np.dot((D*self.layer_op[i-2]).T, p[i-1])

            dc_dw[i-1] = w_gradients[i-1] + reg*(self.weights[i-1])
            db_dw[i-1] = p[i-1]

            if 'nesterov' in self.momemtum_function:
                v_prev = self.momemtum_val[i-1]
                self.momemtum_val[i-1] = self.momemtum*self.momemtum_val[i-1] - alpha*dc_dw[i-1]
                self.weights[i-1] += -self.momemtum*v_prev + (1 + self.momemtum)*self.momemtum_val[i-1]
            if 'simple' in self.momemtum_function:
                self.momemtum_val[i-1] = self.momemtum*self.momemtum_val[i-1] - alpha*dc_dw[i-1]
                self.weights[i-1] += self.momemtum_val[i-1]

            #self.weights[i-1] = self.weights[i-1] - alpha*dc_dw[i-1]
            self.bias[i-1] = self.bias[i-1] - alpha*np.sum(p[i-1], 0)

    def plot_decision_boundary(self):
        """This function plots the decision boundary generated by the NN"""
        contour_qual = 500
        data_min = np.min(self.network_inputs, 0)
        data_max = np.max(self.network_inputs, 0)
        x = np.linspace(data_min[0], data_max[0], contour_qual)
        y = np.linspace(data_min[1], data_max[1], contour_qual)
        X,Y = np.meshgrid(x, y, indexing='xy')
        grid_ip = np.asarray([X.flatten(), Y.flatten()]).T
        grid_op = self.forward(grid_ip, 0)
        grid_op = grid_op.argmax(axis=1) + 1
        grid_op = np.reshape(grid_op, (contour_qual, contour_qual))

        colormap = plt.cm.brg #nipy_spectral, Set1,Paired
        colors = [colormap(i) for i in np.linspace(0, 1, self.num_op_dims)]

        plt.figure()
        plt.contourf(X, Y, grid_op)
        for i in range(0, self.num_op_dims):
            loc = (self.network_targets == i + 1).flatten()
            plt.scatter(self.network_inputs[loc, 0], self.network_inputs[loc, 1], \
            color=colors[i], marker='o', s=5, zorder=10, label=str(i+1))
        plt.xlabel('Dimension 1')
        plt.ylabel('Dimension 2')
        plt.title('NN Decision Boundary')
        plt.legend(loc=0)
        plt.show(block=False)
