"""This file is used to store all activation functions. You can generate
your own too!"""
import numpy as np

class ListOfFuncs:
    """All activation functions to be used by NN package"""
    def identity(self, ip, der):
        """Returns the identity output"""
        if der == 0:
            return ip
        else:
            return np.ones(ip.shape, dtype=float)
    def binStep(self, ip, der):
        """Returns the binary step output"""
        if der == 0:
            return np.ndarray.astype(ip >= 0, float)
        else:
            return ip == 0

    def sigmoid(self, ip, der):
        """Returns the sigmoidal output"""
        fX = np.divide(1, 1 + np.exp(-ip))
        if der == 0:
            return fX
        else:
            return fX*(1 - fX)

    def tanh(self, ip, der):
        """Returns the tanh output"""
        if der == 0:
            return np.tanh(ip)
        else:
            return 1 - np.power(np.tanh(ip), 2)

    def relu(self, ip, der):
        """Returns the RELU activation output"""
        if der == 0:
            ip[ip < 0] = 0
            return ip
        else:
            return (ip >= 0).astype(np.float)

    def leaky_relu(self, ip, der):
        """Returns Leaky RELU activation output"""
        if der == 0:
            ip[ip < 0] = 0.01*ip[ip < 0]
            return ip
        else:
            temp = (ip >= 0).astype(np.float)
            temp[temp == 0] = 0.01
            return temp

    def __init__(self):
        print("Imported Activation Functions")

    def showFunctions(self):
        """Prints out all the available function calls"""
        from inspect import getmembers, isfunction
        functions_list = [o for o in getmembers(ListOfFuncs) if isfunction(o[1])]
        print(functions_list)
