import pickle

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
from torch.autograd import Variable

from Phase_1_Model import LSTM_predict

PATH_TO_DATASET = '/media/rakshit/New Volume/Dataset/Event detection/'

DATASET = pickle.load(open(PATH_TO_DATASET + 'Dataset.p', "rb"))

def Generate_Dataset(DATASET, Participant_Index, WinSize):
    '''Given the Type_str as 'Train' or 'Test', this function will generate
    a dataset from the given participant index. For multiple participants,
    provide a list with their index numbers.'''

    Forward_Batches = []
    Labels = []
    Backward_Batches = []
    for i in Participant_Index:
        for j in range(0, len(DATASET[i])):
            temp_batch_forward, temp_batch_backward, temp_label = Get_Batches(DATASET[i][j], WinSize)
            Forward_Batches = Forward_Batches + temp_batch_forward
            Labels = Labels + temp_label
            Backward_Batches = Backward_Batches + temp_batch_backward
    return (Forward_Batches, Backward_Batches, Labels)

def Get_Batches(ip, WinSize):
    """Given input sequence, convert it to batches and return the data"""
    data = np.concatenate((ip["EIHVelocity"], ip["HeadVelocity"], ip["EIHAng_AzEl_vel"]), axis=1)
    labels = ip["Labels"]

    num_samples = np.shape(data)[0]
    forward_batch = []
    backward_batch = []
    label_batch = []
    for i in range(WinSize, num_samples - WinSize + 2):
        x = i - WinSize
        y = i + WinSize - 1
        forward_batch.append(data[x:i, :])
        label_batch.append(labels[i-1, :])
        backward_batch.append(np.flip(data[i-1:y, :], axis=0))
    return (forward_batch, backward_batch, label_batch)

def Divide_Dataset(x, test_per):
    """Given labels, divide the dataset by the given fraction and ensure
    that each class has representation in the testing set"""
    Label_types = np.unique(x)
    train_idx = []
    test_idx = []
    for i in Label_types:
        idx_loc = np.where(x == i)[0]
        L = np.size(idx_loc)
        temp = np.random.permutation(L)
        test_locs = temp[0:int(np.round(test_per*L))]
        train_locs = temp[int(np.round(test_per*L)):]
        test_idx.append(idx_loc[test_locs])
        train_idx.append(idx_loc[train_locs])
    return (np.concatenate(train_idx).astype('int'), np.concatenate(test_idx).astype('int'))

FORWARD, BACKWARD, LABELS = Generate_Dataset(DATASET, [0, 1, 2], 5)
LABELS = np.asarray(LABELS).squeeze()

TRAIN_IDX, TEST_IDX = Divide_Dataset(LABELS, 0.3)

FORWARD_TRAIN = [FORWARD[i] for i in TRAIN_IDX]
BACKWARD_TRAIN = [BACKWARD[i] for i in TRAIN_IDX]
LABELS_TRAIN = [LABELS[i] for i in TRAIN_IDX]

FORWARD_TEST = [FORWARD[i] for i in TEST_IDX]
BACKWARD_TEST = [BACKWARD[i] for i in TEST_IDX]
LABELS_TEST = [LABELS[i] for i in TEST_IDX]

CRITERION = nn.CrossEntropyLoss()
CLASSES = ['GIW Fixation', 'GIW Pursuit', 'GIW Saccade']

'''Forward LSTM modules'''
NET_F = LSTM_predict(4, 1, 10, 3)
OPTIMIZER_F = optim.Adam(NET_F.parameters())

TRAINLOSS = []
TESTLOSS = []
EPOCHS = []

def forwardpass(ip):
    for i in range(0, len(ip)):
        op = NET_F(ip[i].view(-1, 4))
    return op

for epoch in range(100):
    running_loss = 0.0
    Num_Batches = np.size(LABELS_TRAIN)
    idx_shuf = np.random.permutation(Num_Batches)

    for i in range(Num_Batches):
        NET_F.reset()
        f_input = FORWARD_TRAIN[i]
        Target = np.array([LABELS_TRAIN[i] - 1])
        Target = torch.from_numpy(Target).type(torch.LongTensor)
        Target = Variable(Target, requires_grad=False)
        #Target = np.zeros((3, 1), dtype=float)
        #Target[LABELS_TRAIN[i]] = 1
        #Target = Variable(torch.from_numpy(Target).float(), requires_grad=False)
        f_input = Variable(torch.from_numpy(f_input).float(), requires_grad=False)
        OPTIMIZER_F.zero_grad()
        outputs_f = forwardpass(f_input)
        print('Epoch: {}, i: {}, Op: {}, Target: {}'.format(epoch, i, outputs_f.data.numpy(), Target.data.numpy()))
        loss_f = CRITERION(outputs_f, Target)
        loss_f.backward()
        OPTIMIZER_F.step()

        running_loss += loss_f.data[0]

        if i%np.round(Num_Batches/30) == np.round(Num_Batches/30)-1:
            TRAINLOSS.append(running_loss/ np.round(Num_Batches/30))
            EPOCHS.append(epoch + 1)
    print('Epoch: {}. Total Loss: {}'.format(epoch, running_loss))

'''
NET_B = LSTM_predict(4, 5, 16, 3)
OPTIMIZER_B = optim.Adam(NET_B.parameters())

TRAINLOSS = []
TESTLOSS = []
EPOCHS = []

for epoch in range(100):
    running_loss = 0.0
    Num_Batches = np.size(LABELS_TRAIN)
    idx_shuf = np.random.permutation(Num_Batches)

    for i in range(Num_Batches):
        b_input = BACKWARD_TRAIN[i]
        Target = LABELS_TRAIN[i]

        b_input = Variable(torch.from_numpy(b_input))
        OPTIMIZER_B.zero_grad()
        outputs_b = NET_B(b_input)
        loss_b = CRITERION(outputs_b, Target)
        loss_b.backward()
        OPTIMIZER_B.step()

        running_loss += loss_b.data[0]
        if i%np.round(Num_Batches/30) == np.round(Num_Batches/30)-1:
            TRAINLOSS.append(running_loss/ np.round(Num_Batches/30))
            EPOCHS.append(epoch + 1)
'''