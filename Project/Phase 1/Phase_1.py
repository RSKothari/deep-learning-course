import pickle

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from sklearn.metrics import confusion_matrix
from torch.autograd import Variable

from Phase_1_LSTM_Classify import FBCombine, LSTM_classify

PATH_TO_DATASET = '/home/rakshit/Documents/Event detection/'

DATASET = pickle.load(open(PATH_TO_DATASET + 'Dataset.p', "rb"))

def Generate_Dataset(DATASET, Participant_Index, WinSize):
    '''Given the Type_str as 'Train' or 'Test', this function will generate
    a dataset from the given participant index. For multiple participants,
    provide a list with their index numbers.'''

    Forward_Batches = []
    Labels = []
    Backward_Batches = []
    for i in Participant_Index:
        for j in range(0, len(DATASET[i])):
            temp_batch_forward, temp_batch_backward, temp_label = Get_Batches(DATASET[i][j], WinSize)
            Forward_Batches = Forward_Batches + temp_batch_forward
            Labels = Labels + temp_label
            Backward_Batches = Backward_Batches + temp_batch_backward
    return (Forward_Batches, Backward_Batches, Labels)

def Get_Batches(ip, WinSize):
    """Given input sequence, convert it to batches and return the data"""
    data = np.concatenate((ip["EIHVelocity"], ip["HeadVelocity"], ip["EIHAng_AzEl_vel"], ip["HAng_AzEl_vel"]), axis=1)
    labels = ip["Labels"]

    num_samples = np.shape(data)[0]
    forward_batch = []
    backward_batch = []
    label_batch = []
    for i in range(WinSize, num_samples - WinSize + 2):
        x = i - WinSize
        y = i + WinSize - 1
        forward_batch.append(data[x:i, :])
        label_batch.append(labels[i-1, :])
        backward_batch.append(np.flip(data[i-1:y, :], axis=0))
    return (forward_batch, backward_batch, label_batch)

def Divide_Dataset(x, test_per):
    """Given labels, divide the dataset by the given fraction and ensure
    that each class has representation in the testing set"""
    Label_types = np.unique(x)
    train_idx = []
    test_idx = []
    perClass = []
    for i in Label_types:
        idx_loc = np.where(x == i)[0]
        L = np.size(idx_loc)
        perClass.append(L)
        temp = np.random.permutation(L)
        test_locs = temp[0:int(np.round(test_per*L))]
        train_locs = temp[int(np.round(test_per*L)):]
        test_idx.append(idx_loc[test_locs])
        train_idx.append(idx_loc[train_locs])
    return (np.concatenate(train_idx).astype('int'), np.concatenate(test_idx).astype('int'), perClass)

BATCH_SIZE = 7
NUM_FEATURES = 6

FORWARD, BACKWARD, LABELS = Generate_Dataset(DATASET, [0, 1, 2], BATCH_SIZE)
LABELS = np.asarray(LABELS).squeeze()

TRAIN_IDX, TEST_IDX, W = Divide_Dataset(LABELS, 0.3)

FORWARD_TRAIN = [FORWARD[i] for i in TRAIN_IDX]
BACKWARD_TRAIN = [BACKWARD[i] for i in TRAIN_IDX]
LABELS_TRAIN = [LABELS[i] for i in TRAIN_IDX]

FORWARD_TEST = [FORWARD[i] for i in TEST_IDX]
BACKWARD_TEST = [BACKWARD[i] for i in TEST_IDX]
LABELS_TEST = [LABELS[i] for i in TEST_IDX]

W = np.array(W)
W = Variable(torch.Tensor(1 - W/np.sum(W)).float(), requires_grad=False)
CRITERION = nn.CrossEntropyLoss(W)
CLASSES = ['GIW Fixation', 'GIW Pursuit', 'GIW Saccade']
'''
#FORWARD LSTM
BEST_FORWARD = {'mpca': 0.0, 'epoch': 0.0, 'perclass': np.array([0.0,0.0,0.0]), 'model': []}
NET_F = LSTM_classify(NUM_FEATURES, 40, 3, True).cuda()
OPTIMIZER_F = optim.Adam(NET_F.parameters(), weight_decay=1e-5)

EPOCHS = []

L_TRAIN = []
L_TEST = []
MPCA_TEST = []

epoch = 0
count = 0
while epoch < 5000:
    NET_F.train()
    NET_F.zero_grad()
    NET_F.reset(len(FORWARD_TRAIN))
    Num_Batches = np.size(LABELS_TRAIN)
    idx_shuf = np.random.permutation(Num_Batches)

    batch = np.array([FORWARD_TRAIN[i] for i in idx_shuf])
    batch = np.moveaxis(batch, [0, 1], [1, 0])

    f_input = Variable(torch.from_numpy(batch).float(), requires_grad=False).cuda()

    Target = np.array([LABELS_TRAIN[i] for i in idx_shuf]) - 1
    Target = torch.from_numpy(Target).type(torch.LongTensor)
    Target = Variable(Target, requires_grad=False)

    f_output = NET_F(f_input)
    loss_f = CRITERION(f_output.cpu(), Target)
    loss_f.backward()
    OPTIMIZER_F.step()

    L_TRAIN.append(loss_f.data[0])
    EPOCHS.append(epoch)

    NET_F.reset(len(FORWARD_TEST))
    NET_F.eval()
    #Find testing set loss
    batch_test = np.array(FORWARD_TEST)
    batch_test = np.moveaxis(batch_test, [0, 1], [1, 0])
    batch_test = Variable(torch.from_numpy(batch_test).float(), volatile=True).cuda()

    Target_test = np.array(LABELS_TEST) - 1
    labels_test = torch.from_numpy(Target_test).type(torch.LongTensor)
    labels_test = Variable(labels_test, requires_grad=False)

    Op_test = NET_F(batch_test).cpu()
    loss_test = CRITERION(Op_test, labels_test)
    L_TEST.append(loss_test.data[0])

    Op_test = Op_test.data.numpy()
    confusionMat = confusion_matrix(Target_test, np.argmax(Op_test, axis=1))
    perClassAcc = np.diag(confusionMat)/np.sum(confusionMat, 1)
    mpca = np.mean(perClassAcc)
    MPCA_TEST.append(mpca)
    
    if BEST_FORWARD['mpca'] < mpca:
        BEST_FORWARD['mpca'] = mpca
        BEST_FORWARD['epoch'] = epoch
        BEST_FORWARD['perclass'] = perClassAcc
        BEST_FORWARD['model'] = NET_F
        count = 0
    elif BEST_FORWARD['mpca'] > mpca:
        count += 1
    print('Epoch: {}. Loss: {}. Test MPCA: {}. Per Class: {}'.format(epoch, loss_f.data[0], mpca, perClassAcc))
    epoch += 1

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(EPOCHS, L_TRAIN, label='Forward Train')
ax.plot(EPOCHS, L_TEST, label='Forward Test')
ax.plot(EPOCHS, MPCA_TEST, label='Forward Test MPCA')
ax.legend(fontsize=15)
ax.set_xlabel('Epochs')
ax.set_ylabel('Scale')
ax.grid(True)
plt.show()

#Backward LSTM
BEST_BACKWARD = {'mpca': 0.0, 'epoch': 0.0, 'perclass': np.array([0.0, 0.0, 0.0]), 'model': []}
NET_B = LSTM_classify(NUM_FEATURES, 40, 3, True).cuda()
OPTIMIZER_B = optim.Adam(NET_B.parameters(), weight_decay=1e-5)

EPOCHS = []

L_TRAIN = []
L_TEST = []
MPCA_TEST = []

count = 0
epoch = 0
while epoch < 5000 and count < 500:
    NET_B.train()
    NET_B.zero_grad()
    NET_B.reset(len(BACKWARD_TRAIN))
    Num_Batches = np.size(LABELS_TRAIN)
    idx_shuf = np.random.permutation(Num_Batches)

    batch = np.array([BACKWARD_TRAIN[i] for i in idx_shuf])
    batch = np.moveaxis(batch, [0, 1], [1, 0])

    b_input = Variable(torch.from_numpy(batch).float(), requires_grad=False).cuda()

    Target = np.array([LABELS_TRAIN[i] for i in idx_shuf]) - 1
    Target = torch.from_numpy(Target).type(torch.LongTensor)
    Target = Variable(Target, requires_grad=False)

    b_output = NET_B(b_input)
    loss_b = CRITERION(b_output.cpu(), Target)
    loss_b.backward()
    OPTIMIZER_B.step()

    L_TRAIN.append(loss_b.data[0])
    EPOCHS.append(epoch)

    NET_B.reset(len(BACKWARD_TEST))
    NET_B.eval()
    #Find testing set loss
    batch_test = np.array(BACKWARD_TEST)
    batch_test = np.moveaxis(batch_test, [0, 1], [1, 0])
    batch_test = Variable(torch.from_numpy(batch_test).float(), volatile=True).cuda()

    Target_test = np.array(LABELS_TEST) - 1
    labels_test = torch.from_numpy(Target_test).type(torch.LongTensor)
    labels_test = Variable(labels_test, requires_grad=False)

    Op_test = NET_B(batch_test).cpu()
    loss_test = CRITERION(Op_test, labels_test)
    L_TEST.append(loss_test.data[0])

    Op_test = Op_test.data.numpy()
    confusionMat = confusion_matrix(Target_test, np.argmax(Op_test, axis=1))
    perClassAcc = np.diag(confusionMat)/np.sum(confusionMat, 1)
    mpca = np.mean(perClassAcc)
    MPCA_TEST.append(mpca)
    
    if BEST_BACKWARD['mpca'] < mpca:
        BEST_BACKWARD['mpca'] = mpca
        BEST_BACKWARD['epoch'] = epoch
        BEST_BACKWARD['perclass'] = perClassAcc
        BEST_BACKWARD['model'] = NET_B
        count = 0
    elif BEST_BACKWARD['mpca'] > mpca:
        count += 1
    print('Epoch: {}. Loss: {}. Test MPCA: {}. Per Class: {}'.format(epoch, loss_f.data[0], mpca, perClassAcc))
    epoch += 1

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(EPOCHS, L_TRAIN, label='Backward Train')
ax.plot(EPOCHS, L_TEST, label='Backward Test')
ax.plot(EPOCHS, MPCA_TEST, label='Backward Test MPCA')
ax.legend(fontsize=15)
ax.set_xlabel('Epochs')
ax.set_ylabel('Scale')
ax.grid(True)
plt.show()

pickle.dump((NET_F, NET_B, BEST_FORWARD, BEST_BACKWARD), open("Trained classifiers.p", "wb"))
'''
# Combine Forward and Backward LSTMs
BEST_COMBINATION = {'mpca': 0.0, 'epoch': 0.0, 'perclass': np.array([0.0, 0.0, 0.0]), 'model': []}
_, _, BEST_FORWARD, BEST_BACKWARD = pickle.load(open("Trained classifiers.p", "rb"))
NET_F = BEST_FORWARD['model']
NET_B = BEST_BACKWARD['model']

# Remove final Softmax layer from Models
NET_F.comb = False
NET_B.comb = False

NET_F.cuda()
NET_B.cuda()

if len(FORWARD_TRAIN) == len(BACKWARD_TRAIN):
    print("Forward and Backward have the same training size. Good.")
else:
    print('Forward and Backward should have same size.')

NET_CONNECT = FBCombine(BATCH_SIZE*NUM_FEATURES + 3, 48, 3).cuda()
OPTIMIZER_C = optim.Adam(NET_CONNECT.parameters(), weight_decay=1e-5)

TRAINLOSS = []
TESTLOSS = []
EPOCHS = []

L_TRAIN = []
L_TEST = []
MPCA_TEST = []

count = 0
epoch = 0
while epoch < 5000 and count < 500:
    NET_F.reset(len(FORWARD_TRAIN))
    NET_B.reset(len(BACKWARD_TRAIN))
    NET_F.eval()
    NET_B.eval()
    NET_CONNECT.train()
    NET_F.zero_grad()
    NET_B.zero_grad()
    NET_CONNECT.zero_grad()
    
    Num_Batches = np.size(LABELS_TRAIN)
    idx_shuf = np.random.permutation(Num_Batches)

    batch_f = np.array([FORWARD_TRAIN[i] for i in idx_shuf])
    batch_b = np.array([BACKWARD_TRAIN[i] for i in idx_shuf])
    batch_f = np.moveaxis(batch_f, [0, 1], [1, 0])
    batch_b = np.moveaxis(batch_b, [0, 1], [1, 0])
    
    f_input = Variable(torch.from_numpy(batch_f).float(), volatile=True).cuda()
    b_input = Variable(torch.from_numpy(batch_b).float(), volatile=True).cuda()

    Target = np.array([LABELS_TRAIN[i] for i in idx_shuf]) - 1
    Target = torch.from_numpy(Target).type(torch.LongTensor)
    Target = Variable(Target, requires_grad=False)
    
    f_output = NET_F(f_input)
    b_output = NET_B(b_input)
    f_input = f_input.permute(1, 0, 2).contiguous().view(-1, BATCH_SIZE*NUM_FEATURES)
    b_input = b_input.permute(1, 0, 2).contiguous().view(-1, BATCH_SIZE*NUM_FEATURES)
    
    f_input.volatile = False
    b_input.volatile = False
    f_output.volatile = False
    b_output.volatile = False

    f_input.requires_grad = False
    b_input.requires_grad = False
    f_output.requires_grad = False
    b_output.requires_grad = False
    
    c_output = NET_CONNECT(torch.cat([f_input, f_output], dim=1), torch.cat([b_input, b_output], dim=1))
    loss_c = CRITERION(c_output.cpu(), Target)
    loss_c.backward()
    OPTIMIZER_C.step()

    L_TRAIN.append(loss_c.data[0])
    EPOCHS.append(epoch)

    NET_F.reset(len(FORWARD_TEST))
    NET_B.reset(len(BACKWARD_TEST))
    NET_F.eval()
    NET_B.eval()
    NET_F.zero_grad()
    NET_B.zero_grad()
    NET_CONNECT.zero_grad()
    NET_CONNECT.eval()

    # Testing
    batch_f = np.array(FORWARD_TEST)
    batch_b = np.array(BACKWARD_TEST)
    batch_f = np.moveaxis(batch_f, [0, 1], [1, 0])
    batch_b = np.moveaxis(batch_b, [0, 1], [1, 0])

    f_input = Variable(torch.from_numpy(batch_f).float(), volatile=True).cuda()
    b_input = Variable(torch.from_numpy(batch_b).float(), volatile=True).cuda()

    Target_test = np.array(LABELS_TEST) - 1
    labels_test = torch.from_numpy(Target_test).type(torch.LongTensor)
    labels_test = Variable(labels_test, volatile=True)

    f_output = NET_F(f_input)
    b_output = NET_B(b_input)
    f_input = f_input.permute(1, 0, 2).contiguous().view(-1, BATCH_SIZE*NUM_FEATURES)
    b_input = b_input.permute(1, 0, 2).contiguous().view(-1, BATCH_SIZE*NUM_FEATURES)
    c_output = NET_CONNECT(torch.cat([f_input, f_output], dim=1), torch.cat([b_input, b_output], dim=1)).cpu()
    loss_test = CRITERION(c_output, labels_test)
    L_TEST.append(loss_test.data[0])

    c_output = c_output.data.numpy()
    confusionMat = confusion_matrix(Target_test, np.argmax(c_output, axis=1))
    perClassAcc = np.diag(confusionMat)/np.sum(confusionMat, 1)
    mpca = np.mean(perClassAcc)
    MPCA_TEST.append(mpca)
    
    if BEST_COMBINATION['mpca'] < mpca:
        BEST_COMBINATION['mpca'] = mpca
        BEST_COMBINATION['epoch'] = epoch
        BEST_COMBINATION['perclass'] = perClassAcc
        BEST_COMBINATION['model'] = NET_CONNECT
        count = 0
    elif BEST_COMBINATION['mpca'] > mpca:
        count += 1
    print('Epoch: {}. Loss: {}. Test MPCA: {}. Per Class: {}'.format(epoch, loss_c.data[0], mpca, perClassAcc))
    epoch += 1

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(EPOCHS, L_TRAIN, label='Combined Train')
ax.plot(EPOCHS, L_TEST, label='Combined Test')
ax.plot(EPOCHS, MPCA_TEST, label='Combined Test MPCA')
ax.legend(fontsize=15)
ax.set_xlabel('Epochs')
ax.set_ylabel('Scale')
ax.grid(True)
plt.show()

pickle.dump((NET_CONNECT, BEST_COMBINATION), open('Combined model.p', 'wb'))
