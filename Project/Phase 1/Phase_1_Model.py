import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

class LSTM_predict(nn.Module):
    def __init__(self, ip_dim, batch_size, hidden_dim, op_dim):
        super(LSTM_predict, self).__init__()
        self.ip_dim = ip_dim
        self.batch_size = batch_size
        self.hidden_dim = hidden_dim
        self.op_dim = op_dim

        self.fcn1 = nn.Parameter(torch.Tensor(self.ip_dim, self.hidden_dim), requires_grad=True)
        self.lstm1 = nn.LSTMCell(self.hidden_dim, self.hidden_dim)
        self.lstm2 = nn.LSTMCell(self.hidden_dim, self.hidden_dim)
        self.fcn2 = nn.Parameter(torch.Tensor(self.hidden_dim, self.op_dim), requires_grad=True)
        self.softmax = nn.Softmax()

    #Initialize weights
        self.fcn1.data.uniform_(-1, 1)
        self.fcn2.data.uniform_(-1, 1)

    def reset(self):
        self.h0 = Variable(torch.zeros(self.batch_size, self.hidden_dim), requires_grad=False)
        self.c0 = Variable(torch.zeros(self.batch_size, self.hidden_dim), requires_grad=False)
        self.h1 = Variable(torch.zeros(self.batch_size, self.hidden_dim), requires_grad=False)
        self.c1 = Variable(torch.zeros(self.batch_size, self.hidden_dim), requires_grad=False)

    def forward(self, x):
        x = torch.mm(x, self.fcn1)
        self.h0, self.c0 = self.lstm1(x, (self.h0, self.c0))
        self.h1, self.c1 = self.lstm2(x, (self.h1, self.c1))
        x = torch.mm(self.h1, self.fcn2)
        x = self.softmax(x)
        return x
