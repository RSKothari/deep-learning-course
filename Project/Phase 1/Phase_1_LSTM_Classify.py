import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F

class LSTM_classify(nn.Module):
    def __init__(self, ip_dim, hidden_dim, op_dim, comb):
        super(LSTM_classify, self).__init__()
        self.ip_dim = ip_dim
        self.hidden_dim = hidden_dim
        self.op_dim = op_dim
        self.num_layers = 4
        self.comb = comb

        #self.conv1 = nn.Conv1d(self.ip_dim, int(self.hidden_dim), 3, 1, 0, 1)
        #self.conv2 = nn.Conv1d(self.hidden_dim, int(2*self.hidden_dim), 3, 1, 0, 1)
        #self.conv3 = nn.Conv1d(int(2*self.hidden_dim), 3*self.hidden_dim, 3, 1, 0, 1)
        self.fcn1 = nn.Linear(self.ip_dim, self.hidden_dim)
        self.fcn2 = nn.Linear(self.hidden_dim, self.hidden_dim)
        self.lstm1 = nn.LSTM(self.hidden_dim, self.hidden_dim, self.num_layers)
        self.lstm2 = nn.LSTM(self.hidden_dim, self.hidden_dim, self.num_layers)
        self.lstm3 = nn.LSTM(self.hidden_dim, self.hidden_dim, self.num_layers)
        self.fcn3 = nn.Linear(3*self.hidden_dim, self.op_dim)
        self.softmax = nn.Softmax()

    def reset(self, batch_size):
        self.h0 = Variable(torch.zeros(self.num_layers, batch_size, self.hidden_dim), requires_grad=False).cuda()
        self.c0 = Variable(torch.zeros(self.num_layers, batch_size, self.hidden_dim), requires_grad=False).cuda()
        self.h1 = Variable(torch.zeros(self.num_layers, batch_size, self.hidden_dim), requires_grad=False).cuda()
        self.c1 = Variable(torch.zeros(self.num_layers, batch_size, self.hidden_dim), requires_grad=False).cuda()
        self.h2 = Variable(torch.zeros(self.num_layers, batch_size, self.hidden_dim), requires_grad=False).cuda()
        self.c2 = Variable(torch.zeros(self.num_layers, batch_size, self.hidden_dim), requires_grad=False).cuda()
    def forward(self, x):
        '''
        x = x.permute(1, 2, 0)
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = x.permute(2, 0, 1)
        '''
        x = F.relu(self.fcn1(x))
        x = F.relu(self.fcn2(x))
        x1, _ = self.lstm1(x, (self.h0, self.c0))
        x2, _ = self.lstm2(x, (self.h1, self.c1))
        x3, _ = self.lstm3(x, (self.h2, self.c2))
        x = torch.cat([x1, x2, x3], dim=2)
        x = F.relu(self.fcn3(x))
        x = x[-1]
        if self.comb:
            x = self.softmax(x)
        return x

class FBCombine(nn.Module):
    def __init__(self, ip_dim, hidden_dim, op_dim):
        super(FBCombine, self).__init__()
        self.ip_dim = ip_dim
        self.op_dim = op_dim
        self.hidden_dim = hidden_dim

        self.fcn3 = nn.Linear(self.ip_dim, self.hidden_dim)
        self.fcn4 = nn.Linear(self.ip_dim, self.hidden_dim)
        self.fcn5 = nn.Linear(self.hidden_dim*2, 72)
        self.fcn6 = nn.Linear(72, self.op_dim)
        self.softmax = nn.Softmax()

    def forward(self, x1, x2):
        x1 = F.relu(self.fcn3(x1))
        x2 = F.relu(self.fcn4(x2))
        x = torch.cat([x1, x2], dim=1)
        x = F.relu(self.fcn5(x))
        x = F.relu(self.fcn6(x))
        x = self.softmax(x)
        return x
