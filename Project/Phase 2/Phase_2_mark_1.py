#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 14 15:07:23 2017

@author: rakshit
"""

import pickle

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from sklearn.metrics import confusion_matrix
from torch.autograd import Variable
import gc
import cv2
from Phase_2_Conv_LSTM import Conv_LSTM

PATH_TO_DATASET = '/home/rakshit/Documents/Event detection/'

DATASET = pickle.load(open(PATH_TO_DATASET + 'Dataset.p', "rb"))

def Generate_Dataset(DATASET, Participant_Index, WinSize):
    '''Given the Type_str as 'Train' or 'Test', this function will generate
    a dataset from the given participant index. For multiple participants,
    provide a list with their index numbers.'''

    Forward_Batches = []
    Labels = []
    Backward_Batches = []
    Path_EyeImages = []
    Path_SceneImages = []

    for i in Participant_Index:
        for j in range(0, len(DATASET[i])):
            temp_batch_forward, temp_batch_backward, temp_label, temp_eyepath, temp_scenepath = Get_Batches(DATASET[i][j], WinSize)
            Forward_Batches = Forward_Batches + temp_batch_forward
            Labels = Labels + temp_label
            Backward_Batches = Backward_Batches + temp_batch_backward
            Path_EyeImages = Path_EyeImages + temp_eyepath
            Path_SceneImages = Path_SceneImages + temp_scenepath
    return (Forward_Batches, Backward_Batches, Labels, Path_EyeImages, Path_SceneImages)

def Get_Batches(ip, WinSize):
    """Given input sequence, convert it to batches and return the data"""
    data = np.concatenate((ip["EIHVelocity"], ip["HeadVelocity"], ip["EIHAng_AzEl_vel"], ip["HAng_AzEl_vel"], ip["EyeFrameNo"], ip["SceneFrameNo"]), axis=1)
    labels = ip["Labels"]

    num_samples = np.shape(data)[0]
    forward_batch = []
    backward_batch = []
    path_to_eye_images = []
    path_to_scene_images = []
    label_batch = []
    for i in range(WinSize, num_samples - WinSize + 2):
        x = i - WinSize
        y = i + WinSize - 1
        forward_batch.append(data[x:i, :])
        label_batch.append(labels[i-1, :])
        backward_batch.append(np.flip(data[i-1:y, :], axis=0))
        path_to_eye_images.append([ip["PathToRightEyeImages"][0]])
        path_to_scene_images.append([ip["PathToSceneImages"][0]])
    return (forward_batch, backward_batch, label_batch, path_to_eye_images, path_to_scene_images)

def Divide_Dataset(x, test_per):
    """Given labels, divide the dataset by the given fraction and ensure
    that each class has representation in the testing set"""
    Label_types = np.unique(x)
    train_idx = []
    test_idx = []
    perClass = []
    for i in Label_types:
        idx_loc = np.where(x == i)[0]
        L = np.size(idx_loc)
        perClass.append(L)
        temp = np.random.permutation(L)
        test_locs = temp[0:int(np.round(test_per*L))]
        train_locs = temp[int(np.round(test_per*L)):]
        test_idx.append(idx_loc[test_locs])
        train_idx.append(idx_loc[train_locs])
    return (np.concatenate(train_idx).astype('int'), np.concatenate(test_idx).astype('int'), perClass)

def readImages(path_to_eyes, path_to_scene, EyeFrameNo, SceneFrameNo):
    splitstr = path_to_eyes[0].split('\\')
    name = splitstr[3]
    tr_no = splitstr[4]
    folder_name = splitstr[5]
    path_to_eyes = PATH_TO_DATASET + name + '/' + tr_no + '/' + folder_name + '/'

    splitstr = path_to_scene[0].split('\\')
    name = splitstr[3]
    tr_no = splitstr[4]
    folder_name = splitstr[5]
    path_to_scene = PATH_TO_DATASET + name + '/' + tr_no + '/' + folder_name + '/'

    eye_images = np.zeros((240, 320, BATCH_SIZE), dtype=float)
    #scene_images = np.zeros((720, 960, BATCH_SIZE), dtype=float)

    for i in range(0, BATCH_SIZE):
        # Read in and Pre-process the image
        I_eye = cv2.imread(path_to_eyes + str(int(EyeFrameNo[i])) + '.jpg')
        I_eye = cv2.cvtColor(I_eye, cv2.COLOR_BGR2GRAY).astype(np.float)
        I_eye = I_eye/255
        '''
        I_scene = cv2.imread(path_to_scene + str(SceneFrameNo[i]) + '.jpg')
        I_scene = I_scene[:,:,::-1].astype(np.float)
        I_scene = I_scene/255
        '''
        eye_images[:, :, i] = I_eye
    return eye_images

def divide_superchunks(idx, superbatch_size):
    Num_superbatches = int(np.floor(len(idx)/superbatch_size))
    op = []
    for i in range(Num_superbatches):
        x = i*superbatch_size
        y = np.min([(i+1)*superbatch_size, len(idx)])
        op.append(idx[x:y])
    return op

BATCH_SIZE = 7
NUM_FEATURES = 6
SUPERBATCH_SIZE = 30

FORWARD, BACKWARD, LABELS, PATH_TO_EYE, PATH_TO_SCENE = Generate_Dataset(DATASET, [0, 1, 2], BATCH_SIZE)
LABELS = np.asarray(LABELS).squeeze()

TRAIN_IDX, TEST_IDX, W = Divide_Dataset(LABELS, 0.3)

FORWARD_TRAIN = [FORWARD[i] for i in TRAIN_IDX]
BACKWARD_TRAIN = [BACKWARD[i] for i in TRAIN_IDX]
LABELS_TRAIN = [LABELS[i] for i in TRAIN_IDX]
PATH_TO_EYE_TRAIN = [PATH_TO_EYE[i] for i in TRAIN_IDX]
PATH_TO_SCENE_TRAIN = [PATH_TO_SCENE[i] for i in TRAIN_IDX]

FORWARD_TEST = [FORWARD[i] for i in TEST_IDX]
BACKWARD_TEST = [BACKWARD[i] for i in TEST_IDX]
LABELS_TEST = [LABELS[i] for i in TEST_IDX]
PATH_TO_EYE_TEST = [PATH_TO_EYE[i] for i in TEST_IDX]
PATH_TO_SCENE_TEST = [PATH_TO_SCENE[i] for i in TEST_IDX]

W = np.array(W)
W = Variable(torch.Tensor(1 - W/np.sum(W)).float(), requires_grad=False)
CRITERION = nn.CrossEntropyLoss(W)
CLASSES = ['GIW Fixation', 'GIW Pursuit', 'GIW Saccade']

NUM_BATCHES = np.size(LABELS_TRAIN)

for epoch in range(1):
    idx_shuf = np.random.permutation(NUM_BATCHES)
    superbatch = divide_superchunks(idx_shuf, SUPERBATCH_SIZE)
    

