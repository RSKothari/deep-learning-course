import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


class Conv_LSTM(nn.Module):
    def __init__(self, ip_dim, num_layers, batch_size, op_dim, comb):
        super(Conv_LSTM, self).__init__()
        self.ip_dim = ip_dim
        self.op_dim = op_dim
        self.num_layers = num_layers
        self.batch_size = batch_size
        self.comb = comb
        self.maxpool = nn.MaxPool2d(2)

        #Block 1
        self.conv1 = nn.Conv2d(self.ip_dim, 4, 3, 1, 1)
        #Block 2
        self.conv2 = nn.Conv2d(4, 8, 3, 1, 1)
        #Block 3
        self.conv3 = nn.Conv2d(8, 16, 3, 1, 1)
        #Block 4
        self.conv4 = nn.Conv2d(16, 32, 3, 1, 1)

        self.fcn1 = nn.Linear(9603, 32)
        self.lstm1 = nn.LSTM(32, 32, self.num_layers)
        self.lstm2 = nn.LSTM(32, 32, self.num_layers)
        self.fcn2 = nn.Linear(64, self.op_dim)

    def reset(self):
        self.h0 = Variable(torch.zeros(self.num_layers, 1, 32), requires_grad=False).cuda()
        self.c0 = Variable(torch.zeros(self.num_layers, 1, 32), requires_grad=False).cuda()
        self.h1 = Variable(torch.zeros(self.num_layers, 1, 32), requires_grad=False).cuda()
        self.c1 = Variable(torch.zeros(self.num_layers, 1, 32), requires_grad=False).cuda()
    
    def forward(self, x, h):
        x = F.relu(self.conv1(x))
        x = self.maxpool(x)
        x = F.relu(self.conv2(x))
        x = self.maxpool(x)
        x = F.relu(self.conv3(x))
        x = self.maxpool(x)
        x = F.relu(self.conv4(x))
        x = self.maxpool(x)
        x = x.view(self.batch_size, -1)
        x = torch.cat([x, h], dim = 1)
        x = self.fcn1(x)
        x = torch.unsqueeze(x, 1)
        x1, _ = self.lstm1(x, (self.h0, self.c0))
        x2, _ = self.lstm2(x, (self.h1, self.c1))
        x1 = torch.squeeze(x1)
        x2 = torch.squeeze(x2)
        x = torch.cat([x1, x2], dim = 1)
        x = self.fcn2(x)
        x = x[-1]
        if self.comb:
            return F.softmax(x, dim=0)
