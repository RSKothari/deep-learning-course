# README #

This repository holds all the solutions to Dr. Christopher Kanan's Deep Learning course. Questions can be found as part of the solutions PDF.

### What is this repository for? ###

This repository is meant for students who wish to obtain a reference while completing their own Deep Learning homework.
Clone command: git clone https://RSKothari@bitbucket.org/RSKothari/deep-learning-course.git

### How do I get set up? ###

Requirements
Language: Python 3.5
Numpy: >1.6
Matplotlib
PyTorch + CUDA >= 8.0

### Who do I talk to? ###

Rakshit Kothari (rsk3900@rit.edu)